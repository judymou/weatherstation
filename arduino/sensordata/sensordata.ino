#include "BMPressure.h"
#include "DHT22.h"
#include "GeigerCounter.h"
#include "OpticalDust.h"
#include "BMA180.h"
#include <Wire.h>
#include <stdio.h>
#include <AndroidAccessory.h>

//Android Accessory object
/*
AndroidAccessory acc("DroidX",
"GeigerCounter", 
"Geiger Counter Accessory",
"1.0",
"http://www.caltech.edu",
"0000000012345678"); */

//-----Variables/Initializers--------
BMPressure sensor;  // SDA to A5, SCL to A4 if on Duemilanove
                    // on Mega ADK: SDA to 20, SCL to 21
DHT22 myDHT22(7);    // data pin to D7
GeigerCounter counter(5);  //data pin to D5
OpticalDust dustSensor(0, 6); //Datapin to A0, output volt to D6
BMA180 accelerometer; // SDA to 20, SCL to 21 
                      //(pin5 on bma180 to SCL, pin7 to SDA)
                      // pin 1 to 3.3V, pin 3 to GND
const int photocellInPin = A2;  
int photocell = photocellInPin;  // connected to A2

//Gas Sensor Pin Inputs/Value Outputs
int pinCH4 = A8; //MQ4 
int pinLPG = A9; //MQ6
int pinCO = A10; //MQ7
int pinH2 = A11; //MQ8

//CO Sensor Additional Variables-----
const int pulseHigh = 255;
const int pulseLow = 71;
const unsigned long timeHigh = 60000;
const unsigned long timeLow = 90000; 
int pwmPin = 11; //digital PWM pin

//-----------Stored Values (12)-----------
long last_temperature = -1;
long last_pressure = -1;
float last_humidity = -1;
long last_opticaldust = -1;
float last_AH = -1;
float last_AV = -1;
long last_photoval = -1;
long last_CH4 = -1;
long last_LPG = -1;
long last_CO = -1;
long last_H2 = -1;
long last_geigerAvgGap = -1;
float last_geigerRate = -1;
float last_geigerStdn = -1;
float data[15];

//Geiger Last Values can be retrieved from its class functions

//----------Timers-------------------
unsigned long time = 0;  //timer to keep track of everything

//----------Main Code-----------------
void setup()
{
    Serial.begin(9600);
    Wire.begin();
    sensor.bmp085Calibration(); //bmp085 intitializer
    accelerometer.initBMA180(); //bma180 initializer 
    pinMode(pwmPin, OUTPUT);
    
    //Serial.println("Initialize Sensors:");
    //Serial.println("\tTemperature (*0.1 deg C)\tPressure (Pa)\tHumidity (%RH)\tOptical Dust\tAccel AH\tAceel AV\tPhotocell\tCH4\tLPG\tCO\tH2\tGeiger aveGap\tGeiger rate (count/s)\tGeiger stdn");
    
    //acc.begin();
}

void loop()
{
  time = millis();
//-----------Temperature/Pressure----------------------------  
  short temperature = sensor.bmp085GetTemperature(sensor.bmp085ReadUT());
  long pressure = sensor.bmp085GetPressure(sensor.bmp085ReadUP());
  last_temperature = temperature;
  last_pressure = pressure;


//-----------Temperature/Humidity-----------------------------
  // The sensor can only be read from every 1-2s, and requires a minimum
  // 2s warm-up after power-on.
  // Current code only displays humidity
  
  DHT22_ERROR_t errorCode;
  errorCode = myDHT22.readData();
  switch(errorCode)
  {
    case DHT_ERROR_NONE:
      last_humidity = myDHT22.getHumidityInt()/10+(myDHT22.getHumidityInt()%10)*0.1;
      
      break;
    case DHT_ERROR_TOOQUICK:
      break;
  }
    
//---------------------Optical Dust------------------------------
  dustSensor.cycleModulation();
  last_opticaldust = dustSensor.getPinVal();
  
//---------------------Accelerometer BMA180----------------------
  accelerometer.readAccel();
  last_AH = accelerometer.getAH();
  last_AV = accelerometer.getAV();
 
//--------------------Photocell----------------------------------
  last_photoval = analogRead(photocellInPin);
  
//--------------------Gas Sensors--------------------------------
  //CH4 Sensor
  last_CH4 = analogRead(pinCH4);     // read analog input pin for CH4 sensor        
 
  //LPG Sensor
  last_LPG = analogRead(pinLPG);    
  
  //CO Sensor
  PWMwriter(pwmPin, pulseHigh, pulseLow, timeHigh, timeLow);
  last_CO = analogRead(pinCO);
  
  //H2 Sensor
  last_H2 = analogRead(pinH2);    
  
//----------------------GeigerCounter----------------------------
  counter.computePick();

//-------------------------Print Lines----------------------------
  //Serial.print(millis()); Serial.print("\t");
  //Serial.print(last_temperature, DEC); Serial.print("\t");
  //Serial.print(last_pressure, DEC); Serial.print("\t");
  //Serial.print(last_humidity); Serial.print("\t");
  //Serial.print(last_opticaldust); Serial.print("\t");
  //Serial.print(last_AH); Serial.print("\t");
  //Serial.print(last_AV); Serial.print("\t");
  //Serial.print(last_photoval); Serial.print("\t");
  //Serial.print(last_CH4); Serial.print("\t");
  //Serial.print(last_LPG); Serial.print("\t");
  //Serial.print(last_CO); Serial.print("\t");
  //Serial.print(last_H2); Serial.print("\t");
  //Serial.print(counter.getAvegap()); Serial.print("\t");
  //Serial.print(counter.getRate()); Serial.print("\t");
  //Serial.print(counter.getStdn()); Serial.println("");
  //Serial.println("hello");
  unsigned long current_time = millis();
  last_geigerAvgGap = counter.getAvegap();
  last_geigerRate = counter.getRate();
  last_geigerStdn = counter.getStdn();
  
  float c_current_time = (float)current_time;
  float c_last_temperature = (float)last_temperature;
  float c_last_pressure = (float)last_pressure;
  float c_last_opticaldust = (float)last_opticaldust;
  float c_last_photoval = (float)last_photoval;
  float c_last_CH4 = (float)last_CH4;
  float c_last_LPG = (float)last_LPG;
  float c_last_CO = (float)last_CO;
  float c_last_H2 = (float)last_H2;
  float c_last_geigerAvgGap = (float)last_geigerAvgGap;
  
  data[0] = c_current_time;
  data[1] = c_last_temperature;
  data[2] = c_last_pressure;
  data[3] = last_humidity;
  data[4] = c_last_opticaldust;
  data[5] = last_AH;
  data[6] = last_AV;
  data[7] = c_last_photoval;
  data[8] = c_last_CH4;
  data[9] = c_last_LPG;
  data[10] = c_last_CO;
  data[11] = c_last_H2;
  data[12] = c_last_geigerAvgGap;
  data[13] = last_geigerRate;
  data[14] = last_geigerStdn;
  /*
  Serial.print(millis()); Serial.print("\t");
  Serial.print(c_last_temperature, DEC); Serial.print("\t");
  Serial.print(c_last_pressure, DEC); Serial.print("\t");
  Serial.print(last_humidity); Serial.print("\t");
  Serial.print(c_last_opticaldust); Serial.print("\t");
  Serial.print(last_AH); Serial.print("\t");
  Serial.print(last_AV); Serial.print("\t");
  Serial.print(c_last_photoval); Serial.print("\t");
  Serial.print(c_last_CH4); Serial.print("\t");
  Serial.print(c_last_LPG); Serial.print("\t");
  Serial.print(c_last_CO); Serial.print("\t");
  Serial.print(c_last_H2); Serial.print("\t");
  Serial.print(counter.getAvegap()); Serial.print("\t");
  Serial.print(counter.getRate()); Serial.print("\t");
  Serial.print(counter.getStdn()); Serial.println("");
  /*Serial.print("data start"); 
  Serial.print(data[0]);
  Serial.print(data[1]);
  Serial.print(data[2]);
  Serial.print(data[3]);
  Serial.print(data[4]);
  Serial.print(data[5]);
  Serial.print(data[6]);
  Serial.print(data[7]);
  Serial.print(data[8]);
  Serial.print(data[9]);
  Serial.print(data[10]);
  Serial.print(data[11]);
  Serial.print(data[12]);
  Serial.print(data[13]);
  Serial.print(data[14]);
  Serial.print(data[15]);
  Serial.print(data[16]);  */
  
  if (Serial.peek() != -1) {
    while (Serial.peek() != -1) {
      Serial.read();
    }
    Serial.write((byte *)data, 60);
  }
  
  
  /* Serial.println("data end"); */
  /*
  if (acc.isConnected())
  {
    acc.write((byte*)data, 60);
    //Serial.println("Finished writing");
  } */
  //for safety
  delay(500);  
}

//---------------------------OTHER FUNCTIONS---------------------
/*
  PWMwriter - creates an alternating current
  outPin = output pin
  pHigh, pLow = high current value, low current value resp. 5V maps to a value of 255, 0V maps to 0. 
  tHigh, tLow = duration of high pulse (pHigh), duration of low pulse (pLow) resp. 
*/
void PWMwriter(int outPin, int pHigh, int pLow, int tHigh, int tLow)
{
  unsigned long time = millis();
  if (time % (tHigh + tLow) < tHigh)
  {
     analogWrite(outPin, pHigh);
  }
  else
  {
    analogWrite(outPin, pLow);// Note: Cannot read from digital pin after writing values!
  }
}


