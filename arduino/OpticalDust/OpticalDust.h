/*
	Sandra Fang
	Optical Dust Sensor library - header file
	Adapted code from: http://www.sparkfun.com/tutorials/253
*/

#ifndef OpticalDust_H
#define OpticalDust_H

#include "Arduino.h"

class OpticalDust
{
	public:
		OpticalDust(int inPin, int outPin);
		int pinVal;
		unsigned long cycleStage;
		void cycleModulation();
		int getPinVal();
		
	private:
		int _pin;
		int _output;
		unsigned long _delayON1;
		unsigned long _delayON2;
		unsigned long _offTime;
		unsigned long _cyclePeriod;
};

#endif