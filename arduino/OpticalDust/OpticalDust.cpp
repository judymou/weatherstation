/*
	Sandra Fang
	Optical Dust Sensor library - source file
	Adapted code from: http://sensorapp.net/?p=479
*/

#include "OpticalDust.h"

/*
	inPin must be analog pin, outPin must be digital pin, delayTime is delay
	(in milliseconds) in addition to the 10ms delay as required by the datasheet.
*/
OpticalDust::OpticalDust(int inPin, int outPin)
{
	_pin = inPin;
	pinVal = -1;
	_output = outPin;
	pinMode(_output,OUTPUT);	
	_delayON1 = 280;
	_delayON2 = 40;
	_offTime = 9680;
	_cyclePeriod = _delayON1 + _delayON2 + _offTime;
}

/*
	Steps in the cycle: 
	Power on LED for 280 microsec
	Take reading
	Wait for 40 microsec
	
	Power off for 9680 microsec
	Poweroff for additional _delay microsec
	
	Returns the reading of the sensor
*/
void OpticalDust::cycleModulation()
{
	// ledPower is any digital pin on the arduino connected to Pin 3 on the sensor
    	digitalWrite(_output,LOW); // power on the LED
    	delayMicroseconds(_delayON1);
    	pinVal=analogRead(_pin); // read the dust value via pin 5 on the sensor connected to A0
    	delayMicroseconds(_delayON2);
    	digitalWrite(_output,HIGH); // turn the LED off
    	delayMicroseconds(_offTime);
 

	/* broken code - don't use
	cycleStage = micros() % _cyclePeriod;
	Serial.println(micros());
	Serial.println(cycleStage);
	
	if (cycleStage < _delayON1)
	{
		Serial.println("on");
		digitalWrite(_output,LOW); //Power ON the LED (circuit uses a PNP resistor w/ neg voltage)
    	}
    	else if (cycleStage >= _delayON1 && cycleStage <= (_delayON2+_delayON1))
    	{
		pinVal = analogRead(_pin);
		
    	}
    	else
    	{
		digitalWrite(_output, HIGH); //Turn OFF the LED
		Serial.println("off");
   	}	
	Serial.println(pinVal);
	*/


}

int OpticalDust::getPinVal()
{
	return pinVal;
}