/*
	BMPressure.h - Barometric Pressure Sensor library header
	Datasheet: http://www.sparkfun.com/products/9694
*/

#include "Arduino.h"
#include "BMPressure.h"
#include "Wire.h"

#define BMP085_ADDRESS 0x77  // I2C address of BMP085

const unsigned char OSS = 0;  // Oversampling Setting

//Constrcutor
BMPressure::BMPressure()
{
}

// Stores all of the bmp085's calibration values into global variables
// Calibration values are required to calculate temp and pressure
// This function should be called at the beginning of the program
void BMPressure::bmp085Calibration()
{
  _ac1 = _bmp085ReadInt(0xAA);
  _ac2 = _bmp085ReadInt(0xAC);
  _ac3 = _bmp085ReadInt(0xAE);
  _ac4 = _bmp085ReadInt(0xB0);
  _ac5 = _bmp085ReadInt(0xB2);
  _ac6 = _bmp085ReadInt(0xB4);
  _b1 = _bmp085ReadInt(0xB6);
  _b2 = _bmp085ReadInt(0xB8);
  _mb = _bmp085ReadInt(0xBA);
  _mc = _bmp085ReadInt(0xBC);
  _md = _bmp085ReadInt(0xBE);
}

// Calculate temperature given ut.
// Value returned will be in units of 0.1 deg C
short BMPressure::bmp085GetTemperature(unsigned int ut)
{
  long x1, x2;
  
  x1 = (((long)ut - (long)_ac6)*(long)_ac5) >> 15;
  x2 = ((long)_mc << 11)/(x1 + _md);
  _b5 = x1 + x2;

  return ((_b5 + 8)>>4);  
}

// Calculate pressure given up
// calibration values must be known
// _b5 is also required so bmp085GetTemperature(...) must be called first.
// Value returned will be pressure in units of Pa.
long BMPressure::bmp085GetPressure(unsigned long up)
{
  long x1, x2, x3, b3, b6, p;
  unsigned long b4, b7;
  
  b6 = _b5 - 4000;
  // Calculate B3
  x1 = (_b2 * (b6 * b6)>>12)>>11;
  x2 = (_ac2 * b6)>>11;
  x3 = x1 + x2;
  b3 = (((((long)_ac1)*4 + x3)<<OSS) + 2)>>2;
  
  // Calculate B4
  x1 = (_ac3 * b6)>>13;
  x2 = (_b1 * ((b6 * b6)>>12))>>16;
  x3 = ((x1 + x2) + 2)>>2;
  b4 = (_ac4 * (unsigned long)(x3 + 32768))>>15;
  
  b7 = ((unsigned long)(up - b3) * (50000>>OSS));
  if (b7 < 0x80000000)
    p = (b7<<1)/b4;
  else
    p = (b7/b4)<<1;
    
  x1 = (p>>8) * (p>>8);
  x1 = (x1 * 3038)>>16;
  x2 = (-7357 * p)>>16;
  p += (x1 + x2 + 3791)>>4;
  
  return p;
}

// Read 1 byte from the BMP085 at 'address'
char BMPressure::_bmp085Read(unsigned char address)
{
  unsigned char data;
  
  Wire.beginTransmission(BMP085_ADDRESS);
  Wire.write(address);
  Wire.endTransmission();
  
  Wire.requestFrom(BMP085_ADDRESS, 1);
  while(!Wire.available())
    ;
    
  return Wire.read();
}

// Read 2 bytes from the BMP085
// First byte will be from 'address'
// Second byte will be from 'address'+1
int BMPressure::_bmp085ReadInt(unsigned char address)
{
  unsigned char msb, lsb;
  
  Wire.beginTransmission(BMP085_ADDRESS);
  Wire.write(address);
  Wire.endTransmission();
  
  Wire.requestFrom(BMP085_ADDRESS, 2);
  while(Wire.available()<2)
    ;
  msb = Wire.read();
  lsb = Wire.read();
  
  return (int) msb<<8 | lsb;
}

// Read the uncompensated temperature value
unsigned int BMPressure::bmp085ReadUT()
{
  unsigned int ut;
  
  // Write 0x2E into Register 0xF4
  // This requests a temperature reading
  Wire.beginTransmission(BMP085_ADDRESS);
  Wire.write(0xF4);
  Wire.write(0x2E);
  Wire.endTransmission();
  
  // Wait at least 4.5ms
  delay(5);
  
  // Read two bytes from registers 0xF6 and 0xF7
  ut = _bmp085ReadInt(0xF6);
  return ut;
}

// Read the uncompensated pressure value
unsigned long BMPressure::bmp085ReadUP()
{
  unsigned char msb, lsb, xlsb;
  unsigned long up = 0;
  
  // Write 0x34+(OSS<<6) into register 0xF4
  // Request a pressure reading w/ oversampling setting
  Wire.beginTransmission(BMP085_ADDRESS);
  Wire.write(0xF4);
  Wire.write(0x34 + (OSS<<6));
  Wire.endTransmission();
  
  // Wait for conversion, delay time dependent on OSS
  delay(2 + (3<<OSS));
  
  // Read register 0xF6 (MSB), 0xF7 (LSB), and 0xF8 (XLSB)
  Wire.beginTransmission(BMP085_ADDRESS);
  Wire.write(0xF6);
  Wire.endTransmission();
  Wire.requestFrom(BMP085_ADDRESS, 3);
  
  // Wait for data to become available
  while(Wire.available() < 3)
    ;
  msb = Wire.read();
  lsb = Wire.read();
  xlsb = Wire.read();
  
  up = (((unsigned long) msb << 16) | ((unsigned long) lsb << 8) | (unsigned long) xlsb) >> (8-OSS);
  
  return up;
}