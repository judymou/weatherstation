/*
  BMPressure.h - Barometric Pressure Sensor library header
  Datasheet: http://www.sparkfun.com/products/9694
*/
#ifndef BMPressure_h
#define BMPressure_h

class BMPressure
{
  public:
    BMPressure();
    short bmp085GetTemperature(unsigned int ut);
	long bmp085GetPressure(unsigned long up);
	unsigned int bmp085ReadUT();
	unsigned long bmp085ReadUP();
	void test();
	void bmp085Calibration();
	
	short temperature;
	long pressure;
  private:
	int _pin;
	
    // Calibration values
	int _ac1;
	int _ac2; 
	int _ac3; 
	unsigned int _ac4;
	unsigned int _ac5;
	unsigned int _ac6;
	int _b1; 
	int _b2;
	int _mb;
	int _mc;
	int _md;
	long _b5; //  b5 is calculated in bmp085GetTemperature(...), 
			 //  this variable is also used in bmp085GetPressure(...)
			 //  so ...Temperature(...) must be called before ...Pressure(...).
	
	
	int _bmp085ReadInt(unsigned char address);
	
	char _bmp085Read(unsigned char address);
};

#endif
