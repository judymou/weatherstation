/*
BMA180 Accelerometer source file- Measures acceleration in the horizontal and vertical axes
Code developed by Dr. Julian Bunn, edited by Sandra Fang
Datasheet: http://www.sparkfun.com/products/9723
*/

#ifndef BMA180_H_
#define BMA180_H_

class BMA180
{
	public:
		BMA180();
		
		int maxX;
		int minX;
		int maxY;
		int minY;
		int maxZ;
		int minZ;
		int sampleNumber;
		float AH;
		float AV;
		int X;
		int Y;
		int Z;
		
		void readAccel();
		void initBMA180();
		float getAV();
		float getAH();
		
	private:
		float _adc2g;
		float _G;
		
		int _writeByte(int channel, int value);
		int _readByte(int channel);
		void _readId();
		
};
		
#endif