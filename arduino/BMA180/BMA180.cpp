/*
BMA180 Accelerometer source file- Measures acceleration in the horizontal and vertical axes
Code developed by Dr. Julian Bunn, edited by Sandra Fang
Datasheet: http://www.sparkfun.com/products/9723
*/

#include <Arduino.h>
#include "Wire.h"
#include "BMA180.h"

#define address 0x40

//constructor
BMA180::BMA180()
{
	maxX = minX = maxY = minY = maxZ = minZ = 0;
	sampleNumber = 0;
	AV, AH = 0;
	_G = -9.81;	// gravity
	_adc2g = 0.125e-3;	// from datasheet: lsb is .125 millig
}

void BMA180::readAccel()
{
	int temp, x, y, z, result;
	int b;
  
	// LSB must be read first, for each axis
	// bit 0 of lsb set means new data
	while(true)
	{
		b = _readByte(0x02);
		if(b & 0x01) break; 
	}
	x = _readByte(0x03);
	x <<= 8; 
	x |= b;
	x >>= 2;
	  
	while(true)
	{
		b = _readByte(0x04);
		if(b & 0x01) break; 
	}
	y = _readByte(0x05);
	y <<= 8; 
	y |= b;
	y >>= 2;

	while(true)
	{
		b = _readByte(0x06);
		if(b & 0x01) break; 
	}
	z = _readByte(0x07);
	z <<= 8;
	z |= b;
	z >>= 2;
  
	//Serial.println(z);
	float a = ((float)x * (float)x) + ((float)y * (float)y);
	AH = sqrt(a)*_adc2g;
	AV = _adc2g*z; 
	X = x; Y = y; Z = z;
	sampleNumber++;
}

void BMA180::initBMA180()
{
	int temp, result, error;
	int b;
  
	//Serial.println("Reset BMA180 and Init");
	_writeByte(0x10,0xB6);
	delay(100);
	b = _readByte(0x00);
	// Chip ID should be 3
	//Serial.print("BMA180 Chip Id = ");
	//Serial.println(b);
  
	b = _readByte(0x01);
	//Serial.print("BMA180 Version = ");
	//Serial.println(b);
  
	temp = _readByte(0x37);
	//Serial.print("Register 0x37 contents (12 bits) = ");
	//Serial.println(temp,BIN);

	b = _readByte(0x0D);
	// Connect to the ctrl_reg1 register and set the ee_w bit to enable writing.
	b |= B00010000;
  
	_writeByte(0x0D, b);
  
	delay(10);

	b = _readByte(0x30);
  
	//Serial.print("Register 0x30 contents (mode config) = ");
	//Serial.println(b,BIN);
  
	b >>= 2;
	b <<= 2;
  //b |= B00000000; // Low Noise Full Bandwidth
  //b |= B00000010; // Low Noise Reduced Power
  //b |= B00000001; // Ultra Low Noise 
	b |= B00000011; // Low Power
  
	//Serial.print("Register 0x30 will be set to = ");
	//Serial.println(b,BIN);  
  
	_writeByte(0x30,b);
	delay(100);
	b = _readByte(0x30);
  
	//Serial.print("Register 0x30 contents after write = ");
	//Serial.println(b);
    
    b = _readByte(0x20);
  
	//Serial.print("Register 0x20 contents = ");
	//Serial.println(b,BIN);    
     
    // Connect to the bw_tcs register and set the filtering level to 10Hz
    // Low pass 10Hz 0000
    // Low pass 20Hz 0001
    // Low pass 1200Hz 0111
    // Band pass 0.2-300 1001
    // High pass 1Hz 1000
    
    // BW bits are upper four
    
    b <<= 4;
    b >>= 4;
    
    b |= B00001111;
    _writeByte(0x20,b);
    delay(100);
    
    b = _readByte(0x20);
    
	//Serial.print("Register 0x20 (bandwidth) contents after write = ");
	//Serial.println(b,BIN);
  
	b = _readByte(0x35);
   
	//Serial.print("Register 0x35 contents = ");
	//Serial.println(b,BIN);    
   
    // Connect to the offset_lsb1 register and set the range to +- 1g
    // these are bits 1,2,3
    
    // preserve high 4 bits
    b >>= 4;
    b <<= 4;
    // OR with the bits we want to set
    // 000 = 1g
    // 001 = 1.5
    // 010 = 2g etc.
    //        ***
    b |= B00000000; 

	_writeByte(0x35,b);
   
	delay(100);
	b = _readByte(0x35);

	//Serial.print("Register 0x35 (range) contents after write = ");
	//Serial.println(b,BIN);    
 
    //Serial.println("BMA180 Init Successful");
}

float BMA180::getAV()
{
	return AV;
}

float BMA180::getAH()
{
	return AH;
}

int BMA180::_writeByte(int channel, int value) 
{
    Wire.beginTransmission(address);
    Wire.write((byte)channel);
    Wire.write((byte)value);
    Wire.endTransmission();
}

int BMA180::_readByte(int channel) 
{
	int result;
	Wire.beginTransmission(address);
	Wire.write((byte)channel);
	Wire.endTransmission();
	Wire.requestFrom(address, 1);
	while(Wire.available())
	{
		result = Wire.read();
	}
	return result;
}

void BMA180::_readId()
{
	byte b = _readByte(0x00);
	Serial.print("Id = ");
	Serial.println(b);
	delay(10);
}