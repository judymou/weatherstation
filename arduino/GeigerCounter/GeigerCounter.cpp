/*
Sandra Fang
GeigerCounter - source file for gathering geiger counter data
Adapted from EunJee Lee
*/
#include "GeigerCounter.h"

GeigerCounter::GeigerCounter(int pin)
{
	_LENGTH = 10;
	_pin = pin;
	pinMode(_pin, INPUT);
	pinMode(13, OUTPUT);

    memset(gap,0,sizeof(gap));
    memset(sd,0,sizeof(sd));
	
	time = 0;
	lastTime = 0;
	_counter = 0;
	_sensorState = 0;
	_lastSensorState = 0;
	_sum = 0;
	_x = 0;
}

void GeigerCounter::computePick()
{
	_sensorState = !digitalRead(_pin);
	digitalWrite(13, _sensorState);
	if (_sensorState != _lastSensorState) 
	{
		if (_sensorState == HIGH) 
		{
			time = millis();
			gap[_x] = time - lastTime;
			//sets timer numbers
			if (_x < 9) 
			{
			 _x++;
			}
			else 
			{
			 _x = 0;
			}
			
			avegap = 0;
            for(int i=0;i<_LENGTH;i++) 
			{
                avegap += gap[i];
            }
            avegap /= _LENGTH;

			for (int i = 0; i < _LENGTH; i++) 
			{
				sd[i]=(gap[i]-avegap)^2;
				_sum = _sum + sd[i];
			}
			rate = 100./(float)avegap;
			stdn = sqrt(_sum/_LENGTH);				
			_counter++;
		}
   }
   _lastSensorState = _sensorState;
   lastTime = time;
}

float GeigerCounter::getRate()
{
	return rate;
}

unsigned long GeigerCounter::getAvegap()
{
	return avegap;
}

double GeigerCounter::getStdn()
{
	return stdn;
}

/*
unsigned long GeigerCounter::getGap(int e)
{
	return gap[e];
}

unsigned long GeigerCounter::getSD(int e)
{
	return sd[e];
}
*/



