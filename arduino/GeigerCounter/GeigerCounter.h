/*
Sandra Fang
GeigerCounter - header for gathering geiger counter data
Adapted from EunJee Lee
*/
#ifndef GeigerCounter_H
#define GeigerCounter_H

#include "Arduino.h"

class GeigerCounter
{
	public:
		GeigerCounter(int pin);
		
		float getRate();
		unsigned long getAvegap();
		double getStdn();
		unsigned long getGap(int e);
		unsigned long getSD(int e);
		void computePick();
		
		unsigned long avegap;
		double stdn;
		float rate;
		unsigned long gap[10];
		unsigned long sd[10];
		unsigned long time;
		unsigned long lastTime;
		
	private:
		int _LENGTH;
		int _pin;
		unsigned int _counter;
		unsigned int _sensorState;
		unsigned int _lastSensorState;
		unsigned long _sum;
		int _x;

};

#endif
