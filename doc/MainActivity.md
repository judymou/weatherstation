# MainActivity

`MainActivity` handles the lifecycles of the Services and manages the UI. It has the following responsibilities:

* It starts up services and stops them when the user quits the application.
* It refreshes views of Fragments using a timer that calls `refreshView` on all Fragments every quarter of a second.
* It calls `rotateView` on the visible Fragments that implement the RotatableFragment interface every 10 seconds, so different view portions can be displayed.
* It determines the order Fragments are visible to users. A different Fragment slides in to take over the central view every 10 seconds.
* It shows corresponding tiles when an alert is received.

Fragments are all optimized such that if no new information is available, `refreshView` does not result in any work. An alternative would be to have different refresh timers for Fragments. It could save function calls, but the code would be more complicated and harder to maintain. `MainActivity` should not be responsible for tracking when new information becomes available for each Fragment.
