Steps to build this project in Eclipse:

# Step 1: Import source code
1. Select File > Import > General > Existing Projects Into Workspace, browse and enter <path>/WeatherStation, and click Finish.

# Step 2: Configure build path
2. Select Project > Properties, select Resource > Android. Under Project Build Target, select Android 4.0.3 as the target. Under Library, make sure google-play-services_lib is included.

3. Select Project > Properties, select Java Build Path, and navigate to Libraries. 

4. Other than including all the jars under libs, the following jar should be included

* Android 4.0.3
* google-play-services-lib.jar in <android-sdk-folder>/extras/google/google_play_services/libproject/google-play-services_lib/bin
* google-play-services.jar in <android-sdk-folder>/extras/google/google_play_services/libproject/google-play-services_lib/libs
* annotations.jar in <android-sdk-folder>/tools/support

# Additional Instruction
If google-play-services_lib is not included, do the following:

1. Select Add, browse and select google-play-services_lib. If no success, do the following.
2. Select File > Import > Android > Existing Android Code Into Workspace and click Next.
3. Select Browse..., enter <android-sdk-folder>/extras/google/google_play_services/libproject/google-play-services_lib, and click Finish.