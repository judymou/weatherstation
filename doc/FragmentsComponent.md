# Fragments
Fragments can be combined to form a multi-tile UI. A Fragment can also be hidden, removed, or reused, which makes it a good candidate for a sliding tile UI. A Service saves data from a stream in a Data Center, while a Fragment is responsible for rendering the UI with the information stored in the Data Center.

## RefreshableFragment
The consumers (e.g. `MainActivity`) of Fragments determine the frequency at which information should be refreshed, whether it is 1 second or 10 minutes. Calling `refreshView` on Fragments with the desired rate (currently every quarter of a second) triggers them to update views if new information is available in Data Centers.

Some Fragments do not need Services to collect data, thus they do not access any Data Centers. These Fragments collect data directly from the source of data to update their views. For example, Fragments that load the CSN website, Bing News, and Weather Heatmap images do not need Services to preprocess data because new information is automatically loaded by accessing URLs. In these cases, `refreshView` is a no-op.

## RotatableFragment
This interface is useful for Fragments that are responsible for displaying large amounts of information because it allows the Fragment to rotate between different portions of the view. When `rotateView` is called on a Fragment, it updates its display but it will not refresh its data from the Data Center. For example, a Map Fragment can move the map horizontally by 20 degrees longitude; a Weather List Fragment can rotate between weather reports for different cities; a News Fragment can rotate between different news story headlines.

![Fragment Component Diagram](https://bitbucket.org/judymou/weatherstation/raw/master/doc/FragmentComponentDiagram.png)