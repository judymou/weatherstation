# System Architecture

The four main components of the application are Services, Data Centers, Fragments, and a cloud communication client. A Service processes incoming data from sensors or other streams. Data Centers store data provided by Services and surface them to Fragments, which render the user interface with the information. The cloud communication client is responsible for communicating with the backend server and encapsulates all the information and logic required for communication. Other components use it to relay data to the cloud. In the current system, Services are the only users of the cloud communication client. The goal is to make the components loosely coupled to increase code readability, reusability, and maintainability.

In the system architecture for the Weather Station application, the dependencies between components are minimized. There are no direct dependencies between data providers (Services) and data consumers (Fragments). Decoupling them allows multiple services to provide data for one fragment or multiple fragments to use one type of data.

The interfaces enforce the minimal communication requirements between components. For example, the main activity requires Fragments to be refreshable and rotatable, and the Fragment needs to check the availability of new data in Data Center. The interfaces will be discussed in more detail in the section below.

![System Architecture Overview Diagram](https://bitbucket.org/judymou/weatherstation/raw/master/doc/SystemArchitectureChart.png)