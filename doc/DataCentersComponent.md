# Data Centers
Data Centers are a group of singleton classes that store data in memory, and provide interfaces for storing and accessing the data. They also provide a Boolean variable indicating if there is new information available that has not been accessed for display.

There are four data centers: `SensorDataCenter`, `EarthquakeDataCenter`, `WeatherDataCenter`,and `EnergyDataCenter`. `SensorDataCenter` stores data from all sensors using a hashmap with the sensor ID as the key and a list for data point storage as the value. The amount of sensor data to keep in memory depends on the time window and resolution (number of samples to be plotted per second).

The storage of different types of data is separated because they require different modification and access mechanisms. For example, the earthquake data stream consists of duplicated quake information, and weather data are refreshed hourly by a service that does not depend on a time window. 

All Data Centers implement the `DataAvailabilityUpdater` interface. Data providers can use the interface to indicate availability of new data, while data consumers can check whether new data is available for display.This interface allows consumers to check for changed data before performing a UI refresh instead of wasting processing time on rerendering old information.

![Data Center Component Diagram](https://bitbucket.org/judymou/weatherstation/raw/master/doc/DataCenterComponentDiagram.png)