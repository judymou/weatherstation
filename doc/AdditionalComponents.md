# Additional components
## Ksigma
This class implements an anomaly detection algorithm that uses the ratio of short-term-average to long-term-average of data samples. Services that perform anomaly detection on incoming data points retrieve a Ksigma instance for each dimension of data. An abnormal value in a stream of data is called a pick. A Ksigma instance also counts the number of picks in a certain time frame. For example, a 3-axis accelerometer requires three Kisgma instances, and a data point is considered to be abnormal if one of the dimensions generates a pick.

## Network time protocol thread
CSN only accepts messages that are time-stamped within 10 seconds of receipt time. The application uses NTP Thread to synchronize time in an Android device. Although syncing with one of the standard public NTP servers would be preferable, better results (increasing consistency between sensor times) are obtained when using a dedicated Caltech NTP server.

## Cloud heartbeat Service
Data from inactive clients do not contribute to event detection. This service sends a heartbeat message to CSN server every 10 minutes to ensure the client is marked as active.

## Weather Station Application class
The Android system creates an instance of the WeatherStationApplication class upon starting, which can be accessed by all activities. It is a good place to store preset configurations. It includes the following information:

* All supporting sensor names and their types. Sensor name is unique across sensors. Each sensor processing Service is associated with a sensor name.
* CSN shakemap website link
* Bing news URLs
* City zip codes and locations (Latitude and Longitude)
