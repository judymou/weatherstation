# Services
A Service is an Android application component that performs long running background operations, which is an ideal platform for data processing. All Services in the application have the following responsibilities:

* At the startup stage, it connects to sensors or datastreams.
* It periodically retrieves data, and the data rate is predetermined by the nature of the information. For example, it processes weather information hourly whereas Phidget data is processed whenever it becomes available, which is fifty times per second.
* It uses Data Center interfaces to store collected data.
* Anomaly detection is performed for each data point if necessary.
  * If an abnormality is detected, it sends an alert to `MainActivity` via an Intent and then requests `CloudClient` to deliver the abnormal data to the server.

Many Services share common resources, so the platform defines and uses two abstract classes, `SensorService` and `StreamDownloaderService`.

## SensorService
This is the base class for the Service that processes sensor data. Since its task includes storing data and sending abnormal data points to the cloud server, the abstract class contains a `SensorDataCenter` instance and a `CloudClient` instance. This abstract class also includes a `Localizer` instance because the device location is required when delivering data to the server. Device opening, closing, and reporting anomalies steps vary across different sensor devices, so they are defined as abstract methods.

## StreamDownloaderService
This is the base class for the Service that downloads information from a data stream (e.g. weather forecast and earthquake RSS feeds). In this case, instead of processing the data points as they become available (usually via a data listener), a timer-triggered task downloads information from a data stream periodically. This abstract class sets up the timer and leaves only the `refreshStreamHelper` method to be implemented by children classes, so Services can perform their own data processing strategy.

![Service Component Diagram](https://bitbucket.org/judymou/weatherstation/raw/master/doc/ServiceComponentDiagram.png)