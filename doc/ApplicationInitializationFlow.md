# Application initialization flow

When the application is started, a chain of Activities is executed. This makes it easy to define dependencies between Activities. For example, the application requires the user to login or register before main application screen will be shown. The Weather Station application currently has the following chain of Activities.

### Activity 1 (`SensorPermissionRequestActivity`)
  * Prompts users to grant permissions for accessing attached sensors. 
### Activity 2 (`RegistrationActivity`)
  * When the application is started for the very first time, it registers sensors on the CSN server. CSN returns a client ID, a secret string, and sensor IDs for each of the registered sensors. This information is required for sending data to the server, and it is stored in persistent storage so that it is available the next time the application is started.
  * With this information, it initializes a CloudClient instance, which is in charge of sending and receiving messages from the server.
### Activity 3 (`LoadingActivity`)
  * The purpose of this activity is to prevent users from seeing a blank screen in the next step. It starts the Services that take time to retrieve data. It will continue to the next step when the data is ready, or it will stay in this activity for a maximum of 10 seconds.
### Activity 4 (`MainActivity`)
  * It starts the main application, which consists of tiles.
