# Weather Station

## Community Seismic Network Client
The Community Seismic Network (CSN) is an open seismic network that allows low cost sensors to contribute to a cloud-based server for aggregation. It analyzes incoming data to determine environmental changes, and thus provides alert of hazardous emergency situations. This application can collect various types of sensor data, send them to the CSN backend server, and display alerts and dynamic information tailored to the users, such as weather forecasts and traffic updates.

## Data Process Platform
WeatherStation is a data process platform for developers to process sensor data and display a collection of dynamic information. The platform should allow the applicationŐs functionality to be easily configurable.

### Configurable tiles
  * Size, orientation, and combination of tiles
  * Display on each tile
  * Tile animation
### Alert triggered display
  * Special events can trigger the main display to change, making the user interface center on the event
### Data processing
  * Easily allows to incorporate additional sensors/data streams/RSS feeds for processing and displaying
### Cloud connection
  * Dedicated activity to perform registration (upon installation of application)
  * Maintain connection and post information to a remote backend server if desired

The platform includes a sliding tile design, which provides a way to display dynamic information with limited screen size. It also defines useful interfaces and abstract classes so that developers can easily incorporate additional data streams into the existing functionalities and UI. Additionally, the platform provides numerous examples of using Services, Fragments, Intents, AysncTasks, Protocol Buffers, Threads, and Timers to aid future developers.

## Design Consideration
Application users have the following on their Android devices:

* Google Play Service
* Google Android Maps version v2
* Android version (4.0.3 Ice-Cream Sandwich or higher)
* The display is the best for tablet (10+ inches screen)

## Quick Start
1. Clone the git repo `git clone https://bitbucket.org/judymou/weatherstation.git`
2. Select Project > Properties, select Resource > Android. Under Project Build
   Target, select Android 4.0.3 as the target. Under Library, make sure
   google-play-services_lib is included.

[Troubleshooting](https://bitbucket.org/judymou/weatherstation/src/master/doc/TroubleShoot.md)

## Documentation
* [System architecture](https://bitbucket.org/judymou/weatherstation/src/master/doc/SystemArchitecture.md)
* [Application initialization flow](https://bitbucket.org/judymou/weatherstation/src/master/doc/ApplicationInitializationFlow.md)
* [Services component](https://bitbucket.org/judymou/weatherstation/src/master/doc/ServicesComponent.md)
* [Data Centers component](https://bitbucket.org/judymou/weatherstation/src/master/doc/DataCentersComponent.md)
* [Cloud client component](https://bitbucket.org/judymou/weatherstation/src/master/doc/CloudClientComponent.md)
* [Fragments component](https://bitbucket.org/judymou/weatherstation/src/master/doc/FragmentsComponent.md)
* [Main Activity](https://bitbucket.org/judymou/weatherstation/src/master/doc/MainActivity.md)
* [Additional components](https://bitbucket.org/judymou/weatherstation/src/master/doc/AdditionalComponents.md)

## Contributing
Anyone and everyone is welcome to download or contribute. Future plan includes

### Configurable settings
  * Instead of having preset cities and news feeds, users should be able to configure these settings to their preferences.

### Phone display
  * Instead of having multiple tile panels in one screen, have one tile at a time for smaller displays.