// NOTE:
// Type "required" is not used in this file intentionally. To be as flexible as
// possible and accommodate unknown future changes, type "required" is being
// avoided. Fields that are currently required by the implementation's data
// checking algorithms are noted in the comments.

package saf;

option java_package = "edu.caltech.saf.messages";
option java_outer_classname = "Heartbeat";

import "common.proto";
import "client_messages.proto";

// Sent once every 10 minutes per client.
// Endpoint: /api/v1/{namespace}/heartbeat/{client-id}
message HeartbeatMessage {
    // REQUIRED: used to verify message origin.
    optional ClientSignature client_signature = 1;

    // OPTIONAL: location if changed since last heartbeat.
    optional double latitude = 2;
    optional double longitude = 3;
    optional LocationSourceType location_source_type = 4;

    // Used to notify the server that the state of the client software has
    // changed.  When not present, the client is presumed alive. When this value
    // is populated, the client is presumed to be going offline for some reason.
    // These types provide an attempt to classify the reason.
    enum StatusChange {
        // Software update, should be temporary.
        UPDATE = 0;
        // Client computer being restarted, put to sleep, or shut down.
        RESTART = 1;
        SLEEP = 2;
        SHUTDOWN = 3;
        // Backend process was killed by a nice kill signal.
        KILLED = 4;
        // Backend process was explicitly quit, e.g. telling the service to stop.
        QUIT = 5;
    }
    // OPTIONAL: indication that client is going offline.
    optional StatusChange status_change = 5;

    // OPTIONAL: ids of sensors that have valid data.
    repeated uint64 connected_sensors = 6;
    // OPTIONAL: ids of sensors that are unavailable.
    repeated uint64 disconnected_sensors = 7;

    // OPTIONAL: id of last received data request.
    optional uint64 latest_request_id = 8;

    // OPTIONAL: data offer for current data. URL is returned if current data
    // is requested. Offer can also be submitted to data/offer, but, if it is
    // known in advance that data will be requested, then making the offer in
    // the heartbeat saves a roundtrip. Clients should continue to offer data
    // in the heartbeat for sensors that continue to receive data requests for
    // ongoing data.
    repeated SensorReadingDescription current_data_offers = 9;

    message SensorEventRate {
        // REQUIRED: id of sensor.
        optional uint64 sensor_id = 1;

        // REQUIRED: event count over last window seconds time window.
        // Used to determine noisiness of sensor.
        optional int32 event_count = 2;
        optional int32 time_window = 3;
    }
    // REQUIRED: current event rate of client's sensors.
    repeated SensorEventRate event_rates = 10;
}

// Indicates the success of the heartbeat and provides requests / updates to
// the client.
message HeartbeatResponse {
    // REQUIRED: indicates success of submission or its error status.
    optional StatusMessage status = 1;

    // Used to request that the client upload data to the server.
    message DataRequest {
        // REQUIRED: sensor from which data is requested.
        optional uint64 sensor_id = 1;

        // REQUIRED: indicates whether request is date based or ongoing.
        optional bool provide_current_data = 2;
        // REQUIRED if provide_current_data is false: id of the data request.
        optional uint64 request_id = 3;

        // REQUIRED if provide_current_data is false: date range of request.
        // Dates are ISO date format in UTC -> yyyy-MM-ddTHH:mm:ss.SSS
        optional string start_date = 4;
        optional string end_date = 5;

        // OPTIONAL: if specified, defines a distance from a central point or
        // points that a client must be within in order to answer the request.
        // If multiple points are provided, they define line segments. Clients
        // can optionally compute their distance to the closest segment, if
        // possible, or, lacking that, their distance to the closest vertex.
        repeated double latitude = 6;
        repeated double longitude = 7;
        optional uint32 threshold_distance = 8;
    }
    // OPTIONAL: used to request timeframes of data from the client.
    repeated DataRequest data_requests = 2;

    // REQUIRED: used to identify the most recent data request in the system.
    // Data requests will not be returned to clients that do not have
    // sensors that match the request, so this field marks the most recent
    // request in the system and should be replayed in heartbeats to determine
    // whether or not newer requests exist.
    optional uint64 latest_request_id = 3;

    // OPTIONAL: if current data was offered, the upload URL will be returned
    // here. If current data was requested, but not offered, the offer will
    // have to be provided to data/offer.
    repeated string current_data_upload_urls = 4;

    // OPTIONAL: when set to true, implies that client should access the
    // metadata handler to see the client's new metadata. Functions as a dirty
    // bit.
    optional bool metadata_changed = 5;
}

message SensorReadingDescription {
    // REQUIRED: id of sensor responding to the request.
    optional uint64 sensor_id = 1;

    // REQUIRED one of current_data or request_id.
    optional bool current_data = 2;
    optional uint64 request_id = 3;

    // REQUIRED: starting and ending time of the provided data.
    // Dates are ISO date format in UTC -> yyyy-MM-ddTHH:mm:ss.SSS
    optional string start_date = 4;
    optional string end_date = 5;

    // OPTIONAL: required for mobile sensors. Starting location for sensor.
    // Required for any sensor that had a different position at the time of
    // data acquisition than it does now.
    optional double latitude = 6;
    optional double longitude = 7;

    enum DataFormat {
        CSNV2 = 0;
        DATA_RECORD = 1;
    }
    // REQUIRED: describes the format of the data that will be uploaded.
    optional DataFormat data_format = 8;
}

// Used to offer data to the server; if the data is desired, an upload URL will
// be returned in response.
// Endpoint: /api/v1/{namespace}/data/offer/{client-id}
message SensorReadingOfferMessage {
    // REQUIRED: used to verify message origin.
    optional ClientSignature client_signature = 1;

    // REQUIRED: one or more descriptions of data being offered.
    repeated SensorReadingDescription sensor_readings = 2;
}

// Returns upload URLs for offered data.
message SensorReadingOfferResponse {
    // REQUIRED: indicates success of submission or its error status.
    optional StatusMessage status = 1;

    // REQUIRED on success: list of strings of length equal to the number of
    // readings described and corresponding to the offers exactly. The correct
    // URL for each reading must be used, as the URL embeds the metadata
    // provided in the offer. An upload URL is provided if the data is desired,
    // otherwise the empty string is returned.
    // For instance, if 4 sensors are attached, and 4 offers for current data
    // are made, but only data from the 1st sensor is desired, if the offer
    // looks like [sensor_id=0, sensor_id=1, sensor_id=2, sensor_id=3], then
    // the response will look like [url, '', '', ''].
    repeated string data_upload_urls = 2;
}

// Returned after a file is uploaded.
message SensorReadingUploadResponse {
    // REQUIRED: indicates success of submission or its error status.
    optional StatusMessage status = 1;

    // REQUIRED on success: the datastore id of the metadata for the uploaded
    // file. Can be used to redownload the file.
    optional uint64 sensor_reading_id = 2;
}

// Used to encode a large sequence of readings.
message DataStream {
    // REQUIRED: used to indicate values from the entire data interval.
    repeated double values = 1 [packed=true];
}

// One possible format for submitting a sequence of readings.
message DataRecord {
    // REQUIRED: Describes data point spacing in milliseconds. At 50 samples per
    // second, this would be 20. At 1 sample per minute, it would be 60,000.
    // NOTE: int32 milliseconds is ~25 days, which should be enough.
    optional uint32 datapoint_spacing = 1;

    // REQUIRED: each record occurs at record_spacing distance apart, with the
    // first record occurring at start_date and the second record occurring at
    // start_date + datapoint_spacing milliseconds. Each DataStream contains the
    // set of values for the entire data interval. That is, a 3-axis
    // accelerometer would send 3 DataStream objects, each of which would be of
    // the exact same length and record a different axis.
    repeated DataStream streams = 2;

    // OPTIONAL: used by mobile clients to indicate different positions during
    // data stream. Location changes are indicated as millisecond offsets from
    // the data start time and are packed into uint32 and double arrays to save
    // space.
    message LocationChanges {
        repeated uint32 time_offsets = 1 [packed=true];
        repeated double latitudes = 2 [packed=true];
        repeated double longitudes = 3 [packed=true];
    }
    optional LocationChanges locations = 3;
}
