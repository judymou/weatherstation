package edu.caltech.android.views;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.SortedSet;

import edu.caltech.android.datacenters.SensorDataCenter;
import edu.caltech.android.extra.SensorInfo;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

/**
 * This view graphs data from multiple sensors into waveforms.
 */
public class WaveformView extends View {

    private final int DEFAULT_WINDOWSIZE = 60000;
    private Paint paint;
    private Paint paintText;

    private final int red = Color.parseColor("#FE642E");
    private final int blue = Color.parseColor("#00BFFF");
    private final int green = Color.parseColor("#4B8A08");
    private final int mediumGray = Color.parseColor("#CEF6F5");

    private SensorDataCenter dataCenter;

    // Duration of the waveform shown.
    private int windowMilliseconds;

    private List<Path> paths;
    private List<Long> sensorIds;
    private List<Integer> colors = new ArrayList<Integer>(Arrays.asList(red, green, blue, red,
            green, blue, red, green, blue, red, green, blue));
    private List<String> sensorText = new ArrayList<String>(Arrays.asList("t", "p", "h", "optical",
            "accelH", "accelV", "photoval", "CH4", "LPG", "CO", "H2", "Geiger"));

    public WaveformView(Context context) {
        super(context);
        init();
    }

    public WaveformView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        dataCenter = SensorDataCenter.getInstance();

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setColor(red);

        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeWidth(4);
        paint.setPathEffect(new CornerPathEffect(10));
        paint.setStyle(Paint.Style.STROKE);

        paintText = new Paint();
        paintText.setAntiAlias(true);
        paintText.setDither(true);
        paintText.setStrokeJoin(Paint.Join.ROUND);
        paintText.setStrokeCap(Paint.Cap.ROUND);
        paintText.setStrokeWidth(24);
        paintText.setTextSize(18);

        paths = new ArrayList<Path>();
        setWindowMilliseconds(DEFAULT_WINDOWSIZE);
    }

    public void setWindowMilliseconds(int wSize) {
        windowMilliseconds = wSize;
    }

    public void setSensorIds(List<Long> s) {
        sensorIds = s;
        for (int i = 0; i < s.size(); i++) {
            paths.add(new Path());
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // an optimization: local variables are faster than accessing fields
        final Paint paint = this.paint;

        // Background color
        canvas.drawColor(mediumGray);

        // Parameters of the view.
        final int height = getHeight();
        final int width = getWidth();

        // Conversion factor between acceleration and vertical pixel position
        double eachScreenSize = (height - 0) / 12;
        // conversion factor between milliseconds to horizontal pixel position.
        // Buffer 100 ms off screen.
        double hscale = (width - 0) / (1.0 * windowMilliseconds - 100);

        float verticalStartPath = ((float) eachScreenSize) / 2;
        for (int i = 0; i < sensorIds.size(); i++) {
            Long sensorId = sensorIds.get(i);
            Path sensorPath = paths.get(i);

            SortedSet<SensorInfo> samples = dataCenter.getSamples(sensorId);
            if (samples.isEmpty()) {
                continue;
            }

            sensorPath.reset();
            sensorPath.moveTo(-5, -5);
            long startTime = samples.first().getTime();
            Double valueFirst = dataCenter.getDataInit(sensorId);
            if (valueFirst == null) {
                valueFirst = samples.first().getReading();
                dataCenter.initData(sensorId, valueFirst);
            }
            long endTime = samples.last().getTime();
            double valueLast = samples.last().getReading();

            for (SensorInfo sample : samples) {
                float valuePixel = (float) (verticalStartPath + (sample.getReading() - valueFirst));
                double t = (sample.getTime() - startTime) * hscale;
                float timePixel = (float) (t - 70);
                sensorPath.lineTo(timePixel, valuePixel);
            }
            paint.setColor(colors.get(i));
            canvas.drawPath(sensorPath, paint);

            float timePixel = (float) ((endTime - startTime) * hscale - 70);
            float textPixel = (float) (verticalStartPath + (valueLast - valueFirst));
            canvas.drawText(sensorText.get(i), timePixel, textPixel + 5, paintText);

            verticalStartPath += eachScreenSize;
        }
    }

}