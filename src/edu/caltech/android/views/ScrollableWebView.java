package edu.caltech.android.views;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.webkit.WebView;

/**
 * This Webview allows user to specify a position it wants to scroll to upon
 * loading a page.
 */
public class ScrollableWebView extends WebView {

    private int numHorizontalScroll = 100;
    private int numVerticalScroll = 0;
    private boolean enableScroll = false;

    public ScrollableWebView(Context context) {
        super(context);
    }

    public ScrollableWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ScrollableWebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public ScrollableWebView(Context context, AttributeSet attrs, int defStyle,
            boolean privateBrowsing) {
        super(context, attrs, defStyle, privateBrowsing);
    }

    public void setNumHorizontalScroll(int numHorizontalScroll) {
        this.numHorizontalScroll = numHorizontalScroll;
    }

    public void setNumVerticalScroll(int numVerticalScroll) {
        this.numVerticalScroll = numVerticalScroll;
    }

    public void setEnableScroll(boolean enableScroll) {
        this.enableScroll = enableScroll;
    }

    @Override
    public void invalidate() {
        super.invalidate();

        // Scroll just once. The common usage of this is when the webpage is
        // loaded, scroll to a specific position.
        if (enableScroll && getContentHeight() > 0 && getScrollX() == 0 && getScrollY() == 0) {
            scrollTo(numHorizontalScroll, numVerticalScroll);
        }
    }
}
