package edu.caltech.android.views;

import java.util.SortedSet;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import edu.caltech.android.datacenters.SensorDataCenter;
import edu.caltech.android.extra.SensorInfo;
import edu.caltech.android.extra.SensorInfoThreeValues;

/**
 * View for generating waveform for accelerometers.
 */
public class AccelWaveformView extends View {
    private final String TAG = "AccelWaveformView";

    private final double MAX_ACCEL = 2 * 9.8;
    private final int DEFAULT_WINDOWSIZE = 3000;

    private final int red = Color.parseColor("#CC0000");
    private final int blue = Color.parseColor("#003399");
    private final int green = Color.parseColor("#006600");
    private int mediumGray = Color.parseColor("#56C5FF");

    private SensorDataCenter dataCenter;
    private long sensorId = -1;

    private int windowMilliseconds;
    private Paint paint;
    private Path pathX;
    private Path pathY;
    private Path pathZ;

    public AccelWaveformView(Context context) {
        super(context);
        init(context);
    }

    public AccelWaveformView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public void changeBackgroundColor(String color) {
        mediumGray = Color.parseColor(color);
    }

    public void setWindowMilliseconds(int wSize) {
        windowMilliseconds = wSize;
    }

    public void setSensorId(long s) {
        sensorId = s;
    }

    private void init(Context context) {
        dataCenter = SensorDataCenter.getInstance();

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setColor(red);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setPathEffect(new CornerPathEffect(10));
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(4);

        setWindowMilliseconds(DEFAULT_WINDOWSIZE);
        pathX = new Path();
        pathY = new Path();
        pathZ = new Path();
    }

    @Override
    protected void onDraw(Canvas canvas) {

        // An optimization: local variables are faster than accessing fields
        final Paint paint = this.paint;

        // Background color
        canvas.drawColor(mediumGray);
        // Conversion factor between acceleration and vertical pixel position
        double vscale = (getHeight() - 0) / (2.0 * MAX_ACCEL);
        // Conversion factor between milliseconds to horizontal pixel position.
        // Buffer 100 ms off screen
        double hscale = (getWidth() - 0) / (1.0 * windowMilliseconds - 100);

        SortedSet<SensorInfo> samples = dataCenter.getSamples(sensorId);
        if (samples == null) {
            return;
        } else if (samples.isEmpty()) {
            Log.d(TAG, "samples is empty");
            return;
        }

        // First element is oldest sample
        long startTime = samples.first().getTime();

        // Move paths to first position
        double x = ((SensorInfoThreeValues) samples.first()).getX() * vscale;
        double y = ((SensorInfoThreeValues) samples.first()).getY() * vscale;
        double z = ((SensorInfoThreeValues) samples.first()).getZ() * vscale;
        pathX.reset();
        pathX.moveTo(absoluteX(0), absoluteY((float) x));
        pathY.reset();
        pathY.moveTo(absoluteX(0), absoluteY((float) y));
        pathZ.reset();
        pathZ.moveTo(absoluteX(0), absoluteY((float) z));

        for (SensorInfo sample : samples) {
            x = ((SensorInfoThreeValues) sample).getX() * vscale;
            float xPixel = absoluteY((float) x);

            y = ((SensorInfoThreeValues) sample).getY() * vscale;
            float yPixel = absoluteY((float) y);

            z = ((SensorInfoThreeValues) sample).getZ() * vscale;
            float zPixel = absoluteY((float) z);

            double t = (sample.getTime() - startTime) * hscale;

            float tPixel = absoluteX((float) t);

            pathX.lineTo(tPixel, xPixel);
            pathY.lineTo(tPixel, yPixel);
            pathZ.lineTo(tPixel, zPixel);
        }

        paint.setColor(red);
        canvas.drawPath(pathX, paint);

        paint.setColor(green);
        canvas.drawPath(pathY, paint);

        paint.setColor(blue);
        canvas.drawPath(pathZ, paint);
    }

    private float absoluteX(float x) {
        return x;
    }

    private float absoluteY(float y) {
        return this.getHeight() / 2 + y;
    }

}
