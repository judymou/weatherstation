package edu.caltech.android.weatherstation;

import java.util.Timer;
import java.util.TimerTask;

import edu.caltech.android.datacenters.EarthquakeDataCenter;
import edu.caltech.android.datacenters.WeatherDataCenter;
import edu.caltech.android.fragments.WeatherForecastFragment;
import edu.caltech.android.services.EarthquakeService;
import edu.caltech.android.services.WeatherForecastService;
import edu.caltech.android.weatherstation.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;

/**
 * The purpose of this activity is to prevent users from seeing a blank screen
 * in the next step. It starts the Services that take time to retrieve data. It
 * will continue to the next step when the data is ready, or it will stay in
 * this activity for a maximum of 10 seconds.
 */
public class LoadingActivity extends Activity {
    private static final String TAG = "LoadingActivity";

    private EarthquakeDataCenter earthquakeData;
    private WeatherDataCenter weatherData;

    private boolean earthquakeAvail = false;
    private boolean weatherAvail = false;

    private Timer refreshLoadingTimer;
    private int refreshLoadingFreq = 2000; // Check every 2 seconds
    private Handler refreshLoadingHandler;

    private boolean isInFront = false;
    private int count = 5; // Wait time is 10 seconds max

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading_main);
    }

    @Override
    public void onStart() {
        super.onStart();
        startLoadingServices();
    }

    // Load services that need loading time
    private void startLoadingServices() {
        startService(new Intent(this, EarthquakeService.class));
        startService(new Intent(this, WeatherForecastService.class));

        earthquakeData = EarthquakeDataCenter.getInstance();
        weatherData = WeatherDataCenter.getInstance();

        refreshLoadingHandler = new Handler();
        refreshLoadingTimer = new Timer("refreshLoadingTimer");
        refreshLoadingTimer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                if (isInFront) {
                    refreshLoadingHandler.post(new Runnable() {
                        public void run() {

                            // Check if data is available
                            earthquakeAvail |= earthquakeData.getNewDataAvail();
                            weatherAvail |= weatherData.getNewWeatherInfoAvail();

                            count--;
                            // If we have waited for enough time or data is
                            // ready, start
                            // next step
                            if (count == 0 || (earthquakeAvail && weatherAvail)) {
                                earthquakeData.alertNewData();
                                weatherData.alertNewData();

                                // Start Main activity
                                Intent nextActivityIntent = new Intent(getApplicationContext(),
                                        MainActivity.class);
                                Log.i(TAG, "start main activity");
                                startActivity(nextActivityIntent);
                                finish();
                            }
                        }
                    });
                }
            }
        }, 0, refreshLoadingFreq);
    }

    @Override
    public void onResume() {
        super.onResume();
        isInFront = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        isInFront = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        refreshLoadingHandler.removeCallbacks(null);
        refreshLoadingTimer.cancel();
    }
}
