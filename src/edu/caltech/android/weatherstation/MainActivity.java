package edu.caltech.android.weatherstation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import edu.caltech.android.fragments.AccelWaveformFragment;
import edu.caltech.android.fragments.EarthquakeMapFragment;
import edu.caltech.android.fragments.GroupWaveformFragment;
import edu.caltech.android.fragments.ListViewFragment;
import edu.caltech.android.fragments.WeatherForecastFragment;
import edu.caltech.android.fragments.WeatherHeatMapFragment;
import edu.caltech.android.fragments.WebViewFragment;
import edu.caltech.android.fragments.interfaces.RefreshableFragment;
import edu.caltech.android.fragments.interfaces.RotatableFragment;
import edu.caltech.android.services.AndroidAccelSensorService;
import edu.caltech.android.services.WeatherStationSensorService;
import edu.caltech.android.services.HeartbeatCloudService;
import edu.caltech.android.services.EarthquakeService;
import edu.caltech.android.services.PhidgetSensorService;
import edu.caltech.android.services.WeatherForecastService;
import edu.caltech.android.weatherstation.R;
import edu.caltech.android.weatherstation.RegistrationActivity;
import edu.caltech.android.weatherstation.WeatherStationApplication;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends android.support.v4.app.FragmentActivity {

    private static final String TAG = "MainActivity";

    // ----------------------------- Menu -------------------------------------
    private static final int MAIN_ID = Menu.FIRST;
    private static final int PAUSE_ID = Menu.FIRST + 1;
    private static final int NEWS_ID = Menu.FIRST + 2;
    private static final int QUIT_ID = Menu.FIRST + 3;

    // --------------------------Broadcast Action -----------------------------
    public static String ENERGY_SERVICE_REFRESH_BROADCAST_ACTION = "BasicMapActivity.refreshenergyaction";

    // ----------------------- Application Fragments --------------------------
    // The screen is split to three columns.
    // Fragments on the left column
    private ListViewFragment weatherList;
    private List<Fragment> weatherForecastFragments;
    private GoogleMap trafficMap;

    // Fragments on the center
    private AccelWaveformFragment localAccelFragment;
    private AccelWaveformFragment phidgetAccelFragment;
    private EarthquakeMapFragment earthquakeMapFragment;
    private WebViewFragment pickViewFragment;
    private WeatherHeatMapFragment weatherHeatMapFragment;

    // Fragments on the right
    private WebViewFragment newsViewFragment;
    private GroupWaveformFragment boxSensorFragment;

    // ---------------------- Fragments Categorization ------------------------
    // Fragments that are refreshed periodically
    private List<RefreshableFragment> refreshableFragments;
    // Fragments that are being rotated in the center
    private List<Fragment> centralRotateFragments;
    // Fragments that are not showing unless an event triggers it.
    private List<Fragment> nonAlertCentralFragments;
    // Fragments in the center but not being rotated.
    private List<Fragment> centralNonRotateFragments;

    // --------------------------- UI States ----------------------------------
    private volatile boolean isInFront; // True if the application is visible to
                                        // users.
    private boolean localAccelFragmentShowing = false;
    private boolean phidgetFragmentShowing = false;

    // Indexes for keeping track of UI
    private int centralFragmentIndex = 0;
    private int weatherForecastFragmentIndex = 0;

    // ---------------------------- Timers ------------------------------------
    private Timer refreshViewTimer;
    private int refreshViewFreq = 200; // 0.2 seconds
    private Handler refreshViewHandler;

    private Timer rotateViewTimer;
    private int rotateStartTime = 20000; // 20 seconds
    private int rotateViewFreq = 10000;
    private Handler rotateViewHandler;

    // Indicates the amount of time alert is showing.
    private int accelSensorDisplayTime = 10000; // 10 seconds
    private List<LatLng> cityLatLng;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.basic_demo);

        // --------------------- Starts Services ------------------------------
        startService(new Intent(this, AndroidAccelSensorService.class));
        startService(new Intent(this, WeatherStationSensorService.class));
        startService(new Intent(this, PhidgetSensorService.class));
        startService(new Intent(this, HeartbeatCloudService.class));

        // --------------------- Setup Fragments ------------------------------
        // ------------ Fragments on the left column --------------------------
        FragmentManager fm = getSupportFragmentManager();
        weatherList = (ListViewFragment) fm.findFragmentById(R.id.weather_list_fragment);
        weatherForecastFragments = new ArrayList<Fragment>(Arrays.asList(
                (WeatherForecastFragment) fm.findFragmentById(R.id.city1),
                (WeatherForecastFragment) fm.findFragmentById(R.id.city2),
                (WeatherForecastFragment) fm.findFragmentById(R.id.city3),
                (WeatherForecastFragment) fm.findFragmentById(R.id.city4),
                (WeatherForecastFragment) fm.findFragmentById(R.id.city5),
                (WeatherForecastFragment) fm.findFragmentById(R.id.city6)));
        for (int i = 0; i < weatherForecastFragments.size(); i++) {
            ((WeatherForecastFragment) weatherForecastFragments.get(i)).setViewIndex(i);
        }
        setUpTrafficMap();

        // -------------------- Fragments on center ---------------------------
        localAccelFragment = (AccelWaveformFragment) fm.findFragmentById(R.id.local_accel_fragment);
        localAccelFragment
                .setSensorId(getSharedPreferences(WeatherStationApplication.PREFERENCE, 0).getLong(
                        AndroidAccelSensorService.SENSOR_NAME, -1));

        phidgetAccelFragment = (AccelWaveformFragment) fm
                .findFragmentById(R.id.phidget_accel_fragment);
        phidgetAccelFragment.setSensorId(getSharedPreferences(WeatherStationApplication.PREFERENCE,
                0).getLong(PhidgetSensorService.SENSOR_NAME, -1));
        phidgetAccelFragment.changeColor("#6BA5E7");

        earthquakeMapFragment = (EarthquakeMapFragment) fm
                .findFragmentById(R.id.earthquake_map_fragment);

        pickViewFragment = (WebViewFragment) fm.findFragmentById(R.id.pick_view_fragment);
        pickViewFragment.setOverviewAndWideview(true, true);
        String[] CSN_URLs = { WeatherStationApplication.CSN_URL };
        pickViewFragment.setURLs(CSN_URLs);
        pickViewFragment.enableAutoReload(10);

        weatherHeatMapFragment = (WeatherHeatMapFragment) fm
                .findFragmentById(R.id.heat_map_fragment);

        // ------------------- Fragments on the right -------------------------
        newsViewFragment = (WebViewFragment) fm.findFragmentById(R.id.news_view_fragment);
        newsViewFragment.setURLs(WeatherStationApplication.NEWS_URLS);
        newsViewFragment.enableScrolling();
        newsViewFragment.enableScrollHorizontally();

        boxSensorFragment = (GroupWaveformFragment) fm.findFragmentById(R.id.sensor_box_fragment);
        List<Long> sensorIds = new ArrayList<Long>();
        for (String s : WeatherStationSensorService.SENSOR_NAMES) {
            sensorIds.add(getSharedPreferences(WeatherStationApplication.PREFERENCE, 0).getLong(s,
                    -1));
        }
        boxSensorFragment.setSensorIds(sensorIds);

        // ---------------- Categorize Fragments ------------------------------
        centralRotateFragments = new ArrayList<Fragment>(Arrays.asList(earthquakeMapFragment,
                pickViewFragment, weatherHeatMapFragment));

        nonAlertCentralFragments = new ArrayList<Fragment>(Arrays.asList(earthquakeMapFragment,
                pickViewFragment, weatherHeatMapFragment));

        centralNonRotateFragments = new ArrayList<Fragment>(Arrays.asList(localAccelFragment,
                phidgetAccelFragment));

        refreshableFragments = new ArrayList<RefreshableFragment>(Arrays.asList(
                (RefreshableFragment) weatherList, (RefreshableFragment) localAccelFragment,
                (RefreshableFragment) phidgetAccelFragment,
                (RefreshableFragment) earthquakeMapFragment,
                (RefreshableFragment) boxSensorFragment,
                (RefreshableFragment) newsViewFragment,
                (RefreshableFragment) pickViewFragment,
                (RefreshableFragment) weatherHeatMapFragment));
        for (Fragment w : weatherForecastFragments) {
            refreshableFragments.add((RefreshableFragment) w);
        }

        // -----------------Setting up the correct start up view --------------
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.hide(localAccelFragment);
        ft.hide(phidgetAccelFragment);
        prepareShowSingleFragment(ft, centralRotateFragments, 0);
        prepareShowSingleFragment(ft, weatherForecastFragments, 0);
        ft.commitAllowingStateLoss();

        // Store weather forecast cities
        cityLatLng = ((WeatherStationApplication) getApplication()).getCityLatLng();

        // Start timers
        startTimers();
    }

    private void setUpTrafficMap() {
        // Do a null check to confirm that we have not already instantiated the
        // map.
        if (trafficMap == null) {
            trafficMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(
                    R.id.traffic_map_fragment)).getMap();
            trafficMap.setTrafficEnabled(true);
        }
    }

    private void startTimers() {
        // One time setup
        newsViewFragment.initLoadView();
        pickViewFragment.initLoadView();

        // Continue refreshing
        refreshViewHandler = new Handler();
        refreshViewTimer = new Timer("refreshViewTimer");
        refreshViewTimer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                if (isInFront) {
                    refreshViewHandler.post(new Runnable() {
                        public void run() {
                            for (RefreshableFragment w : refreshableFragments) {
                                w.refreshView();
                            }
                        }
                    });
                }
            }
        }, 0, refreshViewFreq);

        // Continue rotating
        rotateViewHandler = new Handler();
        rotateViewTimer = new Timer("rotateViewTimer");
        rotateViewTimer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                if (isInFront) {
                    rotateViewHandler.post(new Runnable() {
                        public void run() {
                            if (localAccelFragmentShowing == false &&
                                    phidgetFragmentShowing == false) {
                                rotateCentralFragment();
                            }
                            rotateLeftFragment();
                        }
                    });
                }
            }
        }, rotateStartTime, rotateViewFreq);
    }

    private void rotateCentralFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);

        // Hide all non-rotating fragment
        prepareShowSingleFragment(ft, centralNonRotateFragments, centralNonRotateFragments.size());
        // Pick one of the rotating fragments and show it
        prepareShowSingleFragment(ft, centralRotateFragments, centralFragmentIndex);

        ((RotatableFragment) centralRotateFragments.get(centralFragmentIndex)).rotateView();

        centralFragmentIndex++;
        if (centralFragmentIndex >= centralRotateFragments.size()) {
            centralFragmentIndex = 0;
        }

        ft.commitAllowingStateLoss();
    }

    private void rotateLeftFragment() {
        weatherList.rotateView();

        trafficMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                .target(cityLatLng.get(weatherForecastFragmentIndex)).zoom(11).build()));

        // Weather forecast component
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        prepareShowSingleFragment(ft, weatherForecastFragments, weatherForecastFragmentIndex);
        ft.commitAllowingStateLoss();

        weatherForecastFragmentIndex++;
        if (weatherForecastFragmentIndex >= weatherForecastFragments.size()) {
            weatherForecastFragmentIndex = 0;
        }
    }

    private void prepareShowSingleFragment(FragmentTransaction ft, List<Fragment> f, int i) {
        for (int j = 0; j < f.size(); j++) {
            if (j == i) {
                ft.show(f.get(j));
            } else {
                ft.hide(f.get(j));
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        isInFront = true;
        registerBroadcastReceiver();
    }

    private void registerBroadcastReceiver() {
        IntentFilter filter = new IntentFilter(AndroidAccelSensorService.BROADCAST_PICK_ACTION);
        filter.addAction(PhidgetSensorService.BROADCAST_PICK_ACTION);
        registerReceiver(broadcastReceiver, filter);
    }

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (AndroidAccelSensorService.BROADCAST_PICK_ACTION.equals(action)) {
                if (!localAccelFragmentShowing) {
                    localAccelFragmentShowing = true;

                    // Show local accel fragment
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
                    prepareShowSingleFragment(ft, nonAlertCentralFragments,
                            nonAlertCentralFragments.size());
                    ft.show(localAccelFragment);
                    ft.commitAllowingStateLoss();

                    // Make it stop showing and go back to normal state
                    rotateViewHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            localAccelFragmentShowing = false;
                        }
                    }, accelSensorDisplayTime);
                }
            } else if (PhidgetSensorService.BROADCAST_PICK_ACTION.equals(action)) {
                if (!phidgetFragmentShowing) {
                    phidgetFragmentShowing = true;

                    // Show phidget accel fragment
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
                    prepareShowSingleFragment(ft, nonAlertCentralFragments,
                            nonAlertCentralFragments.size());
                    ft.show(phidgetAccelFragment);
                    ft.commitAllowingStateLoss();

                    // Make it stop showing and go back to normal state
                    rotateViewHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            phidgetFragmentShowing = false;
                        }
                    }, accelSensorDisplayTime);
                }
            }
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        isInFront = false;
        refreshViewHandler.removeCallbacksAndMessages(null);
        rotateViewHandler.removeCallbacksAndMessages(null);
        localAccelFragmentShowing = false;
        phidgetFragmentShowing = false;
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        boolean retVal = super.onCreateOptionsMenu(menu);
        menu.add(0, MAIN_ID, 0, "Main");
        menu.add(1, PAUSE_ID, 1, "Pause");
        menu.add(2, NEWS_ID, 2, "News");
        menu.add(3, QUIT_ID, 3, "Quit");

        return retVal;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
        case QUIT_ID:
            Log.i(TAG, "Quit");
            quitApplicationHelper();
            finish();
            break;
        case MAIN_ID:
            Log.i(TAG, "Main");
            localAccelFragmentShowing = false;
            phidgetFragmentShowing = false;
            FragmentTransaction ft_main = getSupportFragmentManager().beginTransaction();
            ft_main.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
            ft_main.show(boxSensorFragment);
            ft_main.commitAllowingStateLoss();
            break;
        case PAUSE_ID:
            localAccelFragmentShowing = true;
            phidgetFragmentShowing = true;
            break;
        case NEWS_ID:
            FragmentTransaction ft_news = getSupportFragmentManager().beginTransaction();
            ft_news.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
            ft_news.hide(boxSensorFragment);
            ft_news.commitAllowingStateLoss();
            break;
        default:
            break;
        }
        return super.onMenuItemSelected(featureId, item);
    }

    private void quitApplicationHelper() {
        Editor prefEditor = getSharedPreferences(
                WeatherStationApplication.PREFERENCE, 0).edit();
        prefEditor.putLong(RegistrationActivity.CLIENT_MESSAGE_ID, 
                ((WeatherStationApplication) getApplication()).getCloudClient().getMessageId());
        
        stopService(new Intent(this, AndroidAccelSensorService.class));
        stopService(new Intent(this, WeatherStationSensorService.class));
        stopService(new Intent(this, EarthquakeService.class));
        stopService(new Intent(this, PhidgetSensorService.class));
        stopService(new Intent(this, WeatherForecastService.class));
        stopService(new Intent(this, HeartbeatCloudService.class));

        ((WeatherStationApplication) getApplication()).stopCloudClient();
        refreshViewTimer.cancel();
        rotateViewTimer.cancel();
    }
}
