package edu.caltech.android.weatherstation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeMap;

import com.google.android.gms.maps.model.LatLng;

import edu.caltech.android.comm.CloudClient;
import edu.caltech.android.extra.City;
import edu.caltech.saf.messages.Common.SensorType;
import android.app.Application;

public class WeatherStationApplication extends Application {
    private static final String TAG = "WeatherStationApplication";

    public static final String PREFERENCE = "CloudClient_Preferences";

    public static final String CSN_URL = "http://seismocs.appspot.com/#picks";
    public static final String[] NEWS_URLS = { "http://www.bing.com/news?q=us+news",
            "http://www.bing.com/news?q=world+news", "http://www.bing.com/news?q=local+news",
            "http://www.bing.com/news?q=entertainment+news",
            "http://www.bing.com/news?q=science+technology+news",
            "http://www.bing.com/news?q=business+news",
            "http://www.bing.com/news?q=political+news", "http://www.bing.com/news?q=sports+news",
            "http://www.bing.com/news?q=health+news", };

    // Mame of the sensor that are supported by this app
    // unique identifier of the sensors
    public static final String UNDEFINED_NAME = "noName";
    public static final String ANDROID_ACCEL = "androidAccel";
    public static final String PHIDGET = "phidget";
    public static final String BOX_ACCEL_H = "boxAccelHorizontal";
    public static final String BOX_ACCEL_V = "boxAccelVertical";
    public static final String BOX_TEMPERATURE = "boxTemperature";
    public static final String BOX_PRESSURE = "boxPressure";
    public static final String BOX_HUMIDITY = "boxHumidity";
    public static final String BOX_OPTICAL = "boxOptical";
    public static final String BOX_PHOTOVOLTAIC = "boxPhotovoltaic";
    public static final String BOX_CH4 = "boxCH4";
    public static final String BOX_LPG = "boxLPG";
    public static final String BOX_CO = "boxCO";
    public static final String BOX_H2 = "boxH2";
    public static final String BOX_GEIGER = "boxGeiger";

    // Supported sensors and their corresponding type
    private ArrayList<String> sensorNames = new ArrayList<String>(Arrays.asList(PHIDGET,
            ANDROID_ACCEL, BOX_ACCEL_H, BOX_ACCEL_V, BOX_TEMPERATURE, BOX_PRESSURE, BOX_HUMIDITY,
            BOX_OPTICAL, BOX_PHOTOVOLTAIC, BOX_CH4, BOX_LPG, BOX_CO, BOX_H2, BOX_GEIGER));
    private ArrayList<SensorType> sensorTypes = new ArrayList<SensorType>(Arrays.asList(
            SensorType.ACCELEROMETER_3_AXIS, SensorType.ACCELEROMETER_3_AXIS,
            SensorType.ACCELEROMETER_1_AXIS, SensorType.ACCELEROMETER_1_AXIS,
            SensorType.TEMPERATURE, SensorType.PRESSURE, SensorType.HUMIDITY, SensorType.OPTICAL,
            SensorType.PHOTOVOLTAIC, SensorType.CH4, SensorType.LPG, SensorType.CO, SensorType.H2,
            SensorType.GEIGER));

    // Cities: pasadena, newyork, seattle, chicago, austin, sf
    // Cityindex has to be consecutive increasing integer starting at zero
    private ArrayList<City> cities = new ArrayList<City>(Arrays.asList(new City(0, 91105,
            new LatLng(34.0522, -118.2428)), new City(1, 10027, new LatLng(40.7142, -74.006)),
            new City(2, 98109, new LatLng(47.61, -122.3)), new City(3, 60608, new LatLng(41.85,
                    -87.65)), new City(4, 78705, new LatLng(31.4484, -97.78)), new City(5, 94015,
                    new LatLng(37.7750, -122.4183))));

    private TreeMap<String, SensorType> sensorConfigs;

    private CloudClient cloudClient;

    @Override
    public void onCreate() {
        super.onCreate();

        sensorConfigs = new TreeMap<String, SensorType>();
        for (int i = 0; i < sensorNames.size(); i++) {
            sensorConfigs.put(sensorNames.get(i), sensorTypes.get(i));
        }
    }

    public CloudClient getCloudClient() {
        return cloudClient;
    }

    public void setCloudClient(CloudClient c) {
        cloudClient = c;
    }

    public void stopCloudClient() {
        cloudClient.stop();
    }

    public SensorType getSensorType(String name) {
        return sensorConfigs.get(name);
    }

    public TreeMap<String, SensorType> getSensorConfigs() {
        return sensorConfigs;
    }

    public ArrayList<City> getCities() {
        return cities;
    }

    public List<LatLng> getCityLatLng() {
        List<LatLng> loc = new ArrayList<LatLng>();
        for (int i = 0; i < cities.size(); i++) {
            loc.add(cities.get(i).getLatLng());
        }
        return loc;
    }

}
