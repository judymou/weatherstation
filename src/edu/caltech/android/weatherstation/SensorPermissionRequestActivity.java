package edu.caltech.android.weatherstation;

import java.util.HashMap;
import java.util.LinkedList;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.util.Log;

/**
 * Prompts users to grant permissions for accessing attached sensors.
 */
public class SensorPermissionRequestActivity extends Activity {

    private static final String TAG = "SensorPermissionRequestActivity";
    private static final String ACTION_USB_PERMISSION = "edu.caltech.android.weatherstation3.requestPermission.action.USB_ACTION";

    private UsbManager mUsbManager;
    private PendingIntent mPermissionIntent;
    private LinkedList<UsbDevice> usbDevicesRequestList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        usbDevicesRequestList = new LinkedList<UsbDevice>();

        setupSensor();
        collectUsbDevices();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!requestDevicePermission()) {
            Intent intent = new Intent(this, RegistrationActivity.class);
            Log.d(TAG, "start registration activity in onStart");
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mUsbReceiver);
    }

    private void setupSensor() {
        mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION),
                0);

        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        registerReceiver(mUsbReceiver, filter);
    }

    private void collectUsbDevices() {
        HashMap<String, UsbDevice> deviceList = mUsbManager.getDeviceList();

        for (String key : deviceList.keySet()) {
            UsbDevice d = deviceList.get(key);
            if (d != null) {
                usbDevicesRequestList.add(d);
            }
        }
    }

    private boolean requestDevicePermission() {
        while (!usbDevicesRequestList.isEmpty()) {

            UsbDevice d = usbDevicesRequestList.poll();
            if (!mUsbManager.hasPermission(d)) {
                mUsbManager.requestPermission(d, mPermissionIntent);
                return true;
            } else {
            }
        }
        return false;
    }

    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();

            if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    UsbDevice device = (UsbDevice) intent
                            .getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (!intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        Log.d(TAG, "permission denied for device " + device);
                    } else {
                        Log.d(TAG, "permission accepted for device " + device);
                    }
                }

                if (!requestDevicePermission()) {
                    Intent nextActivityIntent = new Intent(SensorPermissionRequestActivity.this,
                            RegistrationActivity.class);
                    startActivity(nextActivityIntent);
                    finish();
                }
            }
        }
    };
}
