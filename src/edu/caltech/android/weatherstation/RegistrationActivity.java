package edu.caltech.android.weatherstation;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;

import edu.caltech.android.comm.CloudClient;
import edu.caltech.android.datacenters.SensorDataCenter;
import edu.caltech.android.extra.Utility;
import edu.caltech.saf.messages.ClientMessages.ClientCreateMessage;
import edu.caltech.saf.messages.ClientMessages.ClientCreateResponse;
import edu.caltech.saf.messages.ClientMessages.SensorMetadata;
import edu.caltech.saf.messages.Common.SensorType;
import edu.caltech.saf.messages.Common.StatusType;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.util.Log;

/**
 * When the application is started for the very first time, it registers sensors
 * on the CSN server. CSN returns a client ID, a secret string, and sensor IDs
 * for each of the registered sensors. This information is required for sending
 * data to the server, and it is stored in persistent storage so that it is
 * available the next time the application is started. With this information, it
 * initializes a CloudClient instance, which is in charge of sending and
 * receiving messages from the server.
 */
public class RegistrationActivity extends Activity {
    private static final String TAG = "RegistrationActivity";

    private static final String REGISTER_ADDRESS = "http://csn-server.appspot.com/api/v1/test/client/create";

    public static final String CLIENT_ID = "clientId";
    public static final String CLIENT_SECRET = "clientSecret";
    public static final String SENSOR_IDS = "sensorIds";
    public static final String LATEST_REQUEST_ID = "latestRequestId";
    public static final String CLIENT_MESSAGE_ID = "clientMessageId";

    @Override
    protected void onStart() {
        super.onStart();
        startCloudClient();
    }

    private void startCloudClient() {

        if (((WeatherStationApplication) getApplication()).getCloudClient() == null) {

            SharedPreferences mPrefs = getSharedPreferences(WeatherStationApplication.PREFERENCE, 0);

            // If already registered before, simply use the existing info and
            // start cloudClient. Otherwise, register the client and persist
            // necessary info
            if (mPrefs.contains(CLIENT_ID) && mPrefs.contains(CLIENT_SECRET) &&
                    mPrefs.contains(SENSOR_IDS) && mPrefs.contains(LATEST_REQUEST_ID) &&
                    mPrefs.contains(CLIENT_MESSAGE_ID)) {

                setupCloudClientAndDataCenter();
                startNextActivity();
            } else {
                registerAndStart();
            }
        } else {
            Log.d(TAG, "cloudClient already exist. info are " +
                    ((WeatherStationApplication) getApplication()).getCloudClient().getClientId() +
                    " " +
                    ((WeatherStationApplication) getApplication()).getCloudClient()
                            .getClientSecret());
            Log.d(TAG, "Datacenter exist as well with size " +
                    SensorDataCenter.getInstance().getSize());
            startNextActivity();
        }
    }

    public void registerAndStart() {
        List<SensorMetadata> sensors = new LinkedList<SensorMetadata>();
        TreeMap<String, SensorType> sensorConfigs = ((WeatherStationApplication) getApplication())
                .getSensorConfigs();
        for (SensorType s : sensorConfigs.values()) {
            sensors.add(SensorMetadata.newBuilder().setSensorType(s).build());
        }

        ClientCreateMessage clientCreateMsg = ClientCreateMessage.newBuilder()
                .addAllAttachedSensors(sensors).build();

        new ClientCreateTask().execute(clientCreateMsg);
    }

    private class ClientCreateTask extends AsyncTask<ClientCreateMessage, Void, String> {
        protected String doInBackground(ClientCreateMessage... msgs) {
            String responseString = null;

            ClientCreateMessage msg = msgs[0];
            try {
                InputStream postResult = Utility.performPost(REGISTER_ADDRESS, msg);
                if (postResult != null) {
                    ClientCreateResponse resp = ClientCreateResponse.parseFrom(postResult);

                    // Get client ID, or handle errors
                    if (resp.getStatus().getType() == StatusType.SUCCESS) {
                        long clientId = resp.getClientId();
                        String clientSecret = resp.getClientSecret();
                        List<Long> sensorIds = resp.getSensorIdsList();
                        long latestRequestId = resp.getLatestRequestId();

                        Log.d("RegistrationTask",
                                "Obtained client id: " + clientId + " client secret: " +
                                        clientSecret + " sensor Ids: " + sensorIds.toString() +
                                        " latest request: " + latestRequestId);

                        // Persist the data
                        Editor prefEditor = getSharedPreferences(
                                WeatherStationApplication.PREFERENCE, 0).edit();
                        prefEditor.putLong(CLIENT_ID, clientId);
                        prefEditor.putString(CLIENT_SECRET, clientSecret);
                        prefEditor.putString(SENSOR_IDS, sensorIds.toString());
                        prefEditor.putLong(LATEST_REQUEST_ID, latestRequestId);

                        TreeMap<String, SensorType> sensorConfigs = ((WeatherStationApplication) getApplication())
                                .getSensorConfigs();
                        int i = 0;
                        for (String name : sensorConfigs.keySet()) {
                            prefEditor.putLong(name, sensorIds.get(i));
                            i++;
                        }
                        prefEditor.commit();

                        return "";
                    } else {
                        Log.d("RegistrationTask", "Status: " + resp.getStatus().getType() + " " +
                                resp.getStatus().getMessage());
                    }
                } else {
                    Log.d("RegistrationTask", "Received null msg");
                }

            } catch (IOException e) {
                Log.d("RegistrationTask", Log.getStackTraceString(e));
            }
            return responseString;
        }

        protected void onPostExecute(String result) {
            if (result == null) {
                finish();
            } else {
                setupCloudClientAndDataCenter();
                startNextActivity();
            }
        }
    }

    private void setupCloudClientAndDataCenter() {
        SharedPreferences mPrefs = getSharedPreferences(WeatherStationApplication.PREFERENCE, 0);
        List<Long> sensorIds = Utility.convertStringToList(mPrefs.getString(SENSOR_IDS, "[]"));
        CloudClient cloudClient = new CloudClient(mPrefs.getLong(CLIENT_ID, 0), mPrefs.getString(
                CLIENT_SECRET, ""), sensorIds, mPrefs.getLong(LATEST_REQUEST_ID, 0));
        cloudClient.setMessageId(mPrefs.getInt(CLIENT_MESSAGE_ID, 0));
        ((WeatherStationApplication) getApplication()).setCloudClient(cloudClient);

        // Init DataCenter
        SensorDataCenter.getInstance().init(sensorIds);
    }

    private void startNextActivity() {
        Intent nextActivityIntent = new Intent(this, LoadingActivity.class);
        startActivity(nextActivityIntent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
