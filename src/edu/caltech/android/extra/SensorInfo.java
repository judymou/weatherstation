package edu.caltech.android.extra;

import java.util.ArrayList;
import java.util.List;

public class SensorInfo implements Comparable<SensorInfo> {

    private long time;

    public SensorInfo(long time) {
        super();
        this.time = time;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public double getReading() {
        return getAllReadings().get(0);
    }

    public List<Double> getAllReadings() {
        return new ArrayList<Double>();
    }

    @Override
    public int compareTo(SensorInfo arg0) {
        return (int) (time - ((SensorInfo) arg0).getTime());
    }
}
