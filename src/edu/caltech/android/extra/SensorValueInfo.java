package edu.caltech.android.extra;

public class SensorValueInfo implements Comparable<SensorValueInfo> {
    private long time;
    private float t;
    private float p;
    private float h;
    private float optical;
    private float AH;
    private float AV;
    private float photoval;
    private float CH4;
    private float LPG;
    private float CO;
    private float H2;
    private float gAvgGap;
    private float gRate;
    private float gStd;

    public SensorValueInfo(float[] sensorValues) {
        time = System.currentTimeMillis();
        t = sensorValues[1];
        p = sensorValues[2];
        h = sensorValues[3];
        optical = sensorValues[4];
        AH = sensorValues[5];
        AV = sensorValues[6];
        photoval = sensorValues[7];
        CH4 = sensorValues[8];
        LPG = sensorValues[9];
        CO = sensorValues[10];
        H2 = sensorValues[11];
        gAvgGap = sensorValues[12];
        gRate = sensorValues[13];
        gStd = sensorValues[14];
    }

    public float getAttribute(int i) {
        switch (i) {
        case 0:
            return t;
        case 1:
            return p;
        case 2:
            return h;
        case 3:
            return optical;
        case 4:
            return AH;
        case 5:
            return AV;
        case 6:
            return photoval;
        case 7:
            return CH4;
        case 8:
            return LPG;
        case 9:
            return CO;
        case 10:
            return H2;
        case 11:
            return gAvgGap;
        }
        return 0;
    }

    public String getAttributeText(int i) {
        switch (i) {
        case 1:
            return "t";
        case 2:
            return "p";
        case 3:
            return "h";
        case 4:
            return "optical";
        case 5:
            return "AH";
        case 6:
            return "AV";
        case 7:
            return "photoval";
        case 8:
            return "CH4";
        case 9:
            return "LPG";
        case 10:
            return "CO";
        case 11:
            return "H2";
        case 12:
            return "Geiger";
        }
        return "nothing";
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public float getT() {
        return t;
    }

    public void setT(float t) {
        this.t = t;
    }

    public float getP() {
        return p;
    }

    public void setP(float p) {
        this.p = p;
    }

    public float getH() {
        return h;
    }

    public void setH(float h) {
        this.h = h;
    }

    public float getOptical() {
        return optical;
    }

    public void setOptical(float optical) {
        this.optical = optical;
    }

    public float getAH() {
        return AH;
    }

    public void setAH(float aH) {
        AH = aH;
    }

    public float getAV() {
        return AV;
    }

    public void setAV(float aV) {
        AV = aV;
    }

    public float getPhotoval() {
        return photoval;
    }

    public void setPhotoval(float photoval) {
        this.photoval = photoval;
    }

    public float getCH4() {
        return CH4;
    }

    public void setCH4(float cH4) {
        CH4 = cH4;
    }

    public float getLPG() {
        return LPG;
    }

    public void setLPG(float lPG) {
        LPG = lPG;
    }

    public float getCO() {
        return CO;
    }

    public void setCO(float cO) {
        CO = cO;
    }

    public float getH2() {
        return H2;
    }

    public void setH2(float h2) {
        H2 = h2;
    }

    public float getgAvgGap() {
        return gAvgGap;
    }

    public void setgAvgGap(float gAvgGap) {
        this.gAvgGap = gAvgGap;
    }

    public float getgRate() {
        return gRate;
    }

    public void setgRate(float gRate) {
        this.gRate = gRate;
    }

    public float getgStd() {
        return gStd;
    }

    public void setgStd(float gStd) {
        this.gStd = gStd;
    }

    @Override
    public int compareTo(SensorValueInfo arg0) {
        SensorValueInfo rhs = (SensorValueInfo) arg0;
        long rhsTime = rhs.getTime();
        if (time < rhsTime) {
            return -1;
        } else if (time == rhsTime) {
            return 0;
        } else {
            return 1;
        }
    }
}
