package edu.caltech.android.extra;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SensorInfoThreeValues extends SensorInfo {

    private double x;
    private double y;
    private double z;

    public SensorInfoThreeValues(long time, double xValue, double yValue, double zValue) {
        super(time);
        x = xValue;
        y = yValue;
        z = zValue;
    }

    @Override
    public List<Double> getAllReadings() {
        return new ArrayList<Double>(Arrays.asList(x, y, z));
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setZ(double z) {
        this.z = z;
    }

}
