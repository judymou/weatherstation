package edu.caltech.android.extra;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import edu.caltech.saf.messages.Common.SensorType;

public class SensorInfoOneValue extends SensorInfo {

    private double value;

    public SensorInfoOneValue(long time, double v) {
        super(time);
        value = v;
    }

    @Override
    public List<Double> getAllReadings() {
        return new ArrayList<Double>(Arrays.asList(value));
    }

    public double getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

}
