package edu.caltech.android.extra;

public class WeatherInfo implements Comparable<WeatherInfo> {

    private City cityInfo;
    private String city;
    private String region;
    private String country;
    private String windChill;
    private String windDirection;
    private String windSpeed;
    private String sunrise;
    private String sunset;
    private String conditiontext;
    private String conditiondate;

    public WeatherInfo() {
    }

    public WeatherInfo(City cityInfo, String city, String region, String country, String windChill,
            String windDirection, String windSpeed, String sunrise, String sunset,
            String conditiontext, String conditiondate) {
        super();
        this.cityInfo = cityInfo;
        this.city = city;
        this.region = region;
        this.country = country;
        this.windChill = windChill;
        this.windDirection = windDirection;
        this.windSpeed = windSpeed;
        this.sunrise = sunrise;
        this.sunset = sunset;
        this.conditiontext = conditiontext;
        this.conditiondate = conditiondate;
    }

    public City getCityInfo() {
        return cityInfo;
    }

    public void setCityInfo(City cityInfo) {
        this.cityInfo = cityInfo;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getWindChill() {
        return windChill;
    }

    public void setWindChill(String windChill) {
        this.windChill = windChill;
    }

    public String getWindDirection() {
        return windDirection;
    }

    public void setWindDirection(String windDirection) {
        this.windDirection = windDirection;
    }

    public String getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(String windSpeed) {
        this.windSpeed = windSpeed;
    }

    public String getSunrise() {
        return sunrise;
    }

    public void setSunrise(String sunrise) {
        this.sunrise = sunrise;
    }

    public String getSunset() {
        return sunset;
    }

    public void setSunset(String sunset) {
        this.sunset = sunset;
    }

    public String getConditiontext() {
        return conditiontext;
    }

    public void setConditiontext(String conditiontext) {
        this.conditiontext = conditiontext;
    }

    public String getConditiondate() {
        return conditiondate;
    }

    public void setConditiondate(String conditiondate) {
        this.conditiondate = conditiondate;
    }

    @Override
    public String toString() {
        return city + " " + region + ", " + country + "\n" + "   windChill: " + windChill +
                "\n   windDirection: " + windDirection + "\n   windSpeed: " + windSpeed +
                "\n   sunrise:" + sunrise + "\n   sunset: " + sunset + "\n   condition:" +
                conditiontext + "\n   date: " + conditiondate;
    }

    @Override
    public int compareTo(WeatherInfo arg0) {
        return cityInfo.getIndex() - arg0.getCityInfo().getIndex();
    }
}
