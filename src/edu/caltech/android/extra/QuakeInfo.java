package edu.caltech.android.extra;

import java.text.SimpleDateFormat;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import android.location.Location;

public class QuakeInfo implements Comparable<QuakeInfo> {
    private DateTime date;
    private String details;
    private Location location;
    private double magnitude;
    private String link;
    private static DateTimeFormatter dtf = DateTimeFormat.shortDateTime();;

    public QuakeInfo(DateTime _d, String _det, Location _loc, double _mag, String _link) {
        date = _d;
        details = _det;
        location = _loc;
        magnitude = _mag;
        link = _link;
    }

    public DateTime getDate() {
        return date;
    }

    public String getDetails() {
        return details;
    }

    public Location getLocation() {
        return location;
    }

    public double getMagnitude() {
        return magnitude;
    }

    public String getLink() {
        return link;
    }

    @Override
    public String toString() {
        return "M " + magnitude + ", " + details + ", " +
                date.toString(dtf.withZone(DateTimeZone.forID("America/Los_Angeles"))) + " PST";
    }

    @Override
    public boolean equals(Object other) {
        if (date.equals(((QuakeInfo) other).getDate()) &&
                location.getLatitude() == ((QuakeInfo) other).getLocation().getLatitude() &&
                location.getLongitude() == ((QuakeInfo) other).getLocation().getLongitude()) {
            return true;
        }
        return false;
    }

    @Override
    public int compareTo(QuakeInfo other) {
        if (date.equals(((QuakeInfo) other).getDate())) {
            Location otherLocation = ((QuakeInfo) other).getLocation();
            if (location.equals(otherLocation)) {
                return 0;
            } else if (location.getLatitude() < otherLocation.getLatitude()) {
                return -1;
            } else if (location.getLatitude() > otherLocation.getLatitude()) {
                return 1;
            } else if (location.getLatitude() == otherLocation.getLatitude()) {
                return (int) (location.getLongitude() - otherLocation.getLongitude());
            }
        } else if (date.isBefore(((QuakeInfo) other).getDate())) {
            return -1;
        } else {
            return 1;
        }
        return 0;
    }
}
