package edu.caltech.android.extra;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.util.Log;

import com.google.protobuf.MessageLite;

public class Utility {

    public static List<Long> convertStringToList(String s) {
        if (s.length() == 2) {
            return new ArrayList<Long>();
        }

        List<String> sensorIdStringList = Arrays.asList(s.substring(1, s.length() - 1).split(", "));
        List<Long> sensorIds = new ArrayList<Long>();
        for (String currentS : sensorIdStringList) {
            sensorIds.add(Long.parseLong(currentS));
        }
        return sensorIds;
    }

    public static String createSHA256HashString(String s) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(s.getBytes());
            byte byteData[] = md.digest();
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                String hex = Integer.toHexString(0xff & byteData[i]);
                if (hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            return "";
        }
    }

    public static InputStream performPost(String u, MessageLite msg) {
        URL url;
        try {
            url = new URL(u);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            msg.writeTo(conn.getOutputStream());
            Log.i("performpos",
                    "response code: " + conn.getResponseCode() + " msg:" +
                            conn.getResponseMessage());
            InputStream inputStream;
            if (conn.getResponseCode() >= 400) {
                inputStream = conn.getErrorStream();
            } else {
                inputStream = conn.getInputStream();
            }
            return inputStream;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
