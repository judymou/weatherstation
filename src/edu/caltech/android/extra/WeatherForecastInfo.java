package edu.caltech.android.extra;

public class WeatherForecastInfo implements Comparable<WeatherForecastInfo> {

    // index identify how close the forecast day to the current date.
    private int index;
    private String day;
    private String condition;
    private String conditionCode;
    private String tempLow;
    private String tempHigh;

    public WeatherForecastInfo() {

    }

    public WeatherForecastInfo(int index, String day, String condition, String conditionCode,
            String tempLow, String tempHigh) {
        super();
        this.index = index;
        this.day = day;
        this.condition = condition;
        this.conditionCode = conditionCode;
        this.tempLow = tempLow;
        this.tempHigh = tempHigh;
    }

    public int getIndex() {
        return index;
    }

    public String getDay() {
        return day;
    }

    public String getCondition() {
        return condition;
    }

    public String getConditionCode() {
        return conditionCode;
    }

    public String getTempLow() {
        return tempLow;
    }

    public String getTempHigh() {
        return tempHigh;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public void setConditionCode(String conditionCode) {
        this.conditionCode = conditionCode;
    }

    public void setTempLow(String tempLow) {
        this.tempLow = tempLow;
    }

    public void setTempHigh(String tempHigh) {
        this.tempHigh = tempHigh;
    }

    @Override
    public int compareTo(WeatherForecastInfo another) {
        return index - another.getIndex();
    }

}
