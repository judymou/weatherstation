package edu.caltech.android.extra;

import com.google.android.gms.maps.model.LatLng;

public class City {

    // the index to be displayed in the list
    private int index;
    private int zipcode;
    private LatLng latLng;

    public City(int index, int zipcode, LatLng latLng) {
        super();
        this.index = index;
        this.zipcode = zipcode;
        this.latLng = latLng;
    }

    public int getIndex() {
        return index;
    }

    public int getZipcode() {
        return zipcode;
    }

    public LatLng getLatLng() {
        return latLng;
    }
}
