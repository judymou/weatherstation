package edu.caltech.android.fragments;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import edu.caltech.android.datacenters.EarthquakeDataCenter;
import edu.caltech.android.extra.QuakeInfo;
import edu.caltech.android.fragments.interfaces.RefreshableFragment;
import edu.caltech.android.fragments.interfaces.RotatableFragment;
import edu.caltech.android.weatherstation.R;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.SupportMapFragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Fragment for displaying earthquake map and the recent earthquake list.
 */
public class EarthquakeMapFragment extends Fragment implements OnMarkerClickListener,
        RotatableFragment, RefreshableFragment {
    private static String TAG = "EarthquakeMapFragment";
    private ListView quakeList;
    private GoogleMap quakeMap;

    private EarthquakeDataCenter earthquakeData;

    private List<Polygon> quakePolygons = new ArrayList<Polygon>();
    private List<Marker> quakeMarkers = new ArrayList<Marker>();

    private DateTimeFormatter dtf;
    private BitmapDescriptor bmdd;

    private int mMapScrollLocationX = 30;
    private int mMapScrollLocationY = -100;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        earthquakeData = EarthquakeDataCenter.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.earthquake_map_fragment, container, false);
        quakeList = (ListView) view.findViewById(R.id.quake_list);
        quakeMap = ((SupportMapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        quakeMap.setOnMarkerClickListener(this);
        dtf = DateTimeFormat.shortDateTime();
        bmdd = BitmapDescriptorFactory.fromResource(R.drawable.red);
        earthquakeData.alertNewData(); // Always force it to load the data the
                                       // first time.
        return view;
    }

    private void animateCamera(int mMapScrollLocationX, int mMapScrollLocationY) {
        quakeMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                .target(new LatLng(mMapScrollLocationX, mMapScrollLocationY)).build()));
    }

    public void refreshView() {
        if (earthquakeData.getNewDataAvail()) {
            updateEarthquakeList();
            updateEarthquakeLocation();
        }
    }

    public void rotateView() {
        mMapScrollLocationY -= 40;
        if (mMapScrollLocationY < -180) {
            mMapScrollLocationY = 180;
        }
        animateCamera(mMapScrollLocationX, mMapScrollLocationY);
    }

    public void updateEarthquakeList() {
        ColoredHeaderAdapter adapter = new ColoredHeaderAdapter(getActivity(),
                android.R.layout.simple_list_item_1, earthquakeData.getRecentQuakes());
        quakeList.setAdapter(adapter);
    }

    public void updateEarthquakeLocation() {
        for (Polygon p : quakePolygons) {
            p.remove();
        }
        quakePolygons.clear();
        for (Marker m : quakeMarkers) {
            m.remove();
        }
        quakeMarkers.clear();

        Iterator<QuakeInfo> iter = earthquakeData.getQuakes().iterator();
        while (iter.hasNext()) {
            QuakeInfo q = iter.next();
            Double latE6 = q.getLocation().getLatitude();
            Double lngE6 = q.getLocation().getLongitude();
            float mag = (float) q.getMagnitude();
            DateTime timeMillis = q.getDate();

            PolygonOptions options = new PolygonOptions();
            int numPoints = 400;
            float radius = (float) (Math.pow(2, mag) / Math.PI) / 4;
            double phase = 2 * Math.PI / numPoints;
            for (int i = 0; i <= numPoints; i++) {
                options.add(new LatLng(latE6 + radius * Math.sin(i * phase), lngE6 + radius *
                        Math.cos(i * phase)));
            }
            int fillColor = Color.HSVToColor(127, new float[] { 0, 1, 1 });
            quakePolygons.add(quakeMap.addPolygon(options.strokeWidth(0).strokeColor(Color.BLACK)
                    .fillColor(fillColor)));

            MarkerOptions m = new MarkerOptions()
                    .icon(bmdd)
                    .position(new LatLng(latE6, lngE6))
                    .title("Location: " +
                            q.getDetails() +
                            "\nMagnitude: " +
                            mag +
                            "\nTime: " +
                            timeMillis.toString(dtf.withZone(DateTimeZone
                                    .forID("America/Los_Angeles"))) + " PST");

            Marker marker = quakeMap.addMarker(m);
            quakeMarkers.add(marker);
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return true;
    }

    // -----------------------Color Adaptor----------------------
    private class ColoredHeaderAdapter extends ArrayAdapter<String> {
        // #2672EC
        private int color = Color.parseColor("#6BA5E7"); // blue
        private int colorOther = Color.parseColor("#FFFFFF"); // white

        public ColoredHeaderAdapter(Context context, int textViewResourceId, List<String> objects) {
            super(context, textViewResourceId, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = super.getView(position, convertView, parent);

            // Set the title blue
            if (position == 0) {
                view.setBackgroundColor(color);
            } else {
                view.setBackgroundColor(colorOther);
            }

            return view;
        }
    }
}
