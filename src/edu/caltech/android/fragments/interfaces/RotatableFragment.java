package edu.caltech.android.fragments.interfaces;

/**
 * This interface is useful for Fragments that are responsible for displaying 
 * large amounts of information because it allows the Fragment to rotate 
 * between different portions of the view. When rotateView is called on a 
 * Fragment, it updates its display but it will not refresh its data from the 
 * Data Center. For example, a Map Fragment can move the map horizontally by 
 * 20 degrees longitude; a Weather List Fragment can rotate between weather reports 
 * for different cities; a News Fragment can rotate between different news story 
 * headlines.
 */
public interface RotatableFragment {
    public void rotateView();
}
