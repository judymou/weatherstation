package edu.caltech.android.fragments.interfaces;

/**
 * The consumers (e.g. MainActivity) of Fragments determine the frequency at which 
 * information should be refreshed, whether it is 1 second or 10 minutes. Calling 
 * refreshView on Fragments with the desired rate (currently every quarter of a second) 
 * triggers them to update views if new information is available in Data Centers.
 */
public interface RefreshableFragment {
    public void refreshView();
}