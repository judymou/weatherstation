package edu.caltech.android.fragments;

import java.util.List;

import edu.caltech.android.fragments.interfaces.RefreshableFragment;
import edu.caltech.android.views.AccelWaveformView;
import edu.caltech.android.views.WaveformView;
import edu.caltech.android.weatherstation.R;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Fragment for displaying waveforms of multiple sensor data streams.
 */
public class GroupWaveformFragment extends Fragment implements RefreshableFragment {
    private static String TAG = "GroupSensorWaveformFragment";
    private WaveformView waveformView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.group_waveform_fragment, container, false);
        waveformView = (WaveformView) view.findViewById(R.id.waveView);
        return view;
    }

    public void refreshView() {
        waveformView.postInvalidate();
    }

    public void setSensorIds(List<Long> sensorIds) {
        waveformView.setSensorIds(sensorIds);
    }

}
