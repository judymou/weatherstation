package edu.caltech.android.fragments;

import java.io.InputStream;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;

import edu.caltech.android.extra.QuakeInfo;
import edu.caltech.android.fragments.interfaces.RefreshableFragment;
import edu.caltech.android.fragments.interfaces.RotatableFragment;
import edu.caltech.android.weatherstation.R;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Fragments for displaying weather heatmaps retrieved from wunderground.com.
 */
public class WeatherHeatMapFragment extends Fragment implements RotatableFragment,
        RefreshableFragment {
    private static String TAG = "WeatherHeatMapFragment";
    private ImageView iv;
    private ImageView iv2;
    private TextView tv1;
    private TextView tv2;
    // Temperature, HeatIndex, windchill, humidity, radar, dew point, wind,
    // visibility, snow depth, precipitation, jetStream, airQuality
    private String[] urls = { "http://icons-ak.wunderground.com/data/640x480/2xus_st.gif",
            "http://icons-ak.wunderground.com/data/640x480/2xus_hi.gif",
            "http://icons-ak.wunderground.com/data/640x480/2xus_wc.gif",
            "http://icons-ak.wunderground.com/data/640x480/2xus_rh.gif",
            "http://icons-ak.wunderground.com/data/640x480/2xus_rd.gif",
            "http://icons-ak.wunderground.com/data/640x480/2xus_dp.gif",
            "http://icons-ak.wunderground.com/data/640x480/2xus_ws.gif",
            "http://icons-ak.wunderground.com/data/640x480/2xus_vs.gif",
            "http://icons-ak.wunderground.com/data/640x480/2xus_snow.gif",
            "http://icons-ak.wunderground.com/data/640x480/2xus_nws_precip.gif",
            "http://icons-ak.wunderground.com/data/640x480/2xus_jt.gif", };
    private String[] imageNames = { "Temperature", "Heat Index", "Wind Chill", "Humidity", "Radar",
            "Dew Point", "Winds", "Visibility", "Snow", "Precipitation", "Jet Stream" };
    private Bitmap[] bitmaps;
    private int urlCurr = 0;
    private boolean inRefresh;
    private HeatMapLookupTask lastLookup;

    private Timer weatherHeatMapTimer;
    private TimerTask timerTask;
    private Handler h;
    private int updateFreq = 60; // min

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView");
        View view = inflater.inflate(R.layout.weather_heat_map_fragment, container, false);
        iv = (ImageView) view.findViewById(R.id.heatmap1);
        iv2 = (ImageView) view.findViewById(R.id.heatmap2);
        tv1 = (TextView) view.findViewById(R.id.image_name_1);
        tv2 = (TextView) view.findViewById(R.id.image_name_2);
        bitmaps = new Bitmap[urls.length];
        inRefresh = false;

        weatherHeatMapTimer = new Timer("weatherHeatMapTimer");
        h = new Handler();
        return view;
    }

    private void rotateViewHelper() {
        if (inRefresh == false) {
            if (urlCurr + 1 >= urls.length) {
                urlCurr = 0;
            }
            iv.setImageBitmap(bitmaps[urlCurr]);
            iv2.setImageBitmap(bitmaps[urlCurr + 1]);
            tv1.setText(imageNames[urlCurr]);
            tv2.setText(imageNames[urlCurr + 1]);
            urlCurr += 2;
        }
    }

    @Override
    public void refreshView() {
    };

    @Override
    public void rotateView() {
        rotateViewHelper();
    }

    private class HeatMapLookupTask extends AsyncTask<Void, QuakeInfo, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            inRefresh = true;
            try {
                for (int i = 0; i < urls.length; i++) {
                    bitmaps[i] = BitmapFactory.decodeStream((InputStream) new URL(urls[i])
                            .getContent());
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                inRefresh = false;
            }
            return null;
        }
    }

    public void refreshHeatMap() {
        if (lastLookup == null || lastLookup.getStatus().equals(AsyncTask.Status.FINISHED)) {
            lastLookup = new HeatMapLookupTask();
            lastLookup.execute((Void[]) null);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (timerTask != null) {
            timerTask.cancel();
        }
        h.removeCallbacksAndMessages(null);
    }

    @Override
    public void onResume() {
        super.onResume();
        scheduleUpdate(0, updateFreq * 1000 * 60);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        weatherHeatMapTimer.cancel();
    }

    private void scheduleUpdate(long startTime, long period) {
        timerTask = new TimerTask() {
            public void run() {
                h.post(new Runnable() {
                    public void run() {
                        Log.i(TAG, "Refreshing heat map");
                        refreshHeatMap();
                    }
                });
            }
        };
        weatherHeatMapTimer.scheduleAtFixedRate(timerTask, startTime, period);
    }
}