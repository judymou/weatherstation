package edu.caltech.android.fragments;

import java.util.Timer;
import java.util.TimerTask;

import edu.caltech.android.fragments.interfaces.RefreshableFragment;
import edu.caltech.android.fragments.interfaces.RotatableFragment;
import edu.caltech.android.views.ScrollableWebView;
import edu.caltech.android.weatherstation.R;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Fragment for displaying webpages.
 * This Fragment allows auto scrolling and setting refresh time of the webpage.
 */
public class WebViewFragment extends Fragment implements RotatableFragment, RefreshableFragment {
    private static String TAG = "PickViewFragment";
    private ScrollableWebView webView;

    private Timer timer;
    private TimerTask scrollTimerTask;
    private TimerTask autoReloadTimerTask;
    private Handler h;

    private String[] urls;

    private int currUrlLink = 0;
    private boolean scroll = false;
    private int scrollFreq = 10; // seconds

    private boolean autoReload = false;
    private boolean needReload = true;
    private int reloadTime;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView");
        View view = inflater.inflate(R.layout.web_view_fragment, container, false);
        webView = (ScrollableWebView) view.findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);

        timer = new Timer("newsWindowTimer");
        h = new Handler();
        return view;
    }

    public void setURLs(String[] u) {
        urls = u;
    }

    public void setOverviewAndWideview(boolean OverviewMode, boolean UseWideviewPort) {
        webView.getSettings().setLoadWithOverviewMode(OverviewMode);
        webView.getSettings().setUseWideViewPort(UseWideviewPort);
    }

    public void initLoadView() {
        webView.loadUrl(urls[0]);
    }

    @Override
    public void refreshView() {
    }

    @Override
    public void rotateView() {
        if (needReload) {
            webView.reload();
            needReload = false;
        }
    }

    public void enableScrollHorizontally() {
        /*
         * webView.setWebViewClient(new WebViewClient() {
         * 
         * @Override public void onPageFinished(WebView view, String url) {
         * Log.i(TAG, "scrolling"); webView.scrollBy(100, 0); } });
         */
        webView.setEnableScroll(true);
    }

    public void enableScrolling() {
        scroll = true;
        scheduleScrollingTimer(20000, 10000);
    }

    // Enable scrolling to list of webpages. This feature also includes
    // scrolling within the page
    private void scheduleScrollingTimer(long startTime, long period) {
        scrollTimerTask = new TimerTask() {
            public void run() {
                h.post(new Runnable() {
                    public void run() {
                        boolean pageNotBottom = webView.pageDown(false);
                        if (pageNotBottom == false) {

                            currUrlLink++;
                            if (currUrlLink >= urls.length) {
                                currUrlLink = 0;
                            }
                            webView.loadUrl(urls[currUrlLink]);
                        }
                    }
                });
            }
        };
        timer.scheduleAtFixedRate(scrollTimerTask, startTime, period);
    }

    public void enableAutoReload(int min) {
        autoReload = true;
        reloadTime = min;
        scheduleAutoReloadTimer(reloadTime * 60 * 1000, reloadTime * 60 * 1000);
    }

    private void scheduleAutoReloadTimer(long startTime, long period) {
        autoReloadTimerTask = new TimerTask() {
            public void run() {
                h.post(new Runnable() {
                    public void run() {
                        needReload = true;
                    }
                });
            }
        };
        timer.scheduleAtFixedRate(autoReloadTimerTask, startTime, period);
    }

    @Override
    public void onPause() {
        super.onPause();
        webView.onPause();
        if (scrollTimerTask != null) {
            scrollTimerTask.cancel();
        }
        if (autoReloadTimerTask != null) {
            autoReloadTimerTask.cancel();
        }
        h.removeCallbacksAndMessages(null);
    }

    @Override
    public void onResume() {
        super.onResume();
        webView.onResume();
        if (scroll) {
            scheduleScrollingTimer(40000, scrollFreq * 1000);
        }
        if (autoReload) {
            needReload = true;
            scheduleAutoReloadTimer(reloadTime * 60 * 1000, reloadTime * 60 * 1000);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "Canceling Timer");
        webView.getSettings().setJavaScriptEnabled(false);
        timer.cancel();
    }
}