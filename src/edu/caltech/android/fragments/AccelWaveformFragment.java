package edu.caltech.android.fragments;

import edu.caltech.android.fragments.interfaces.RefreshableFragment;
import edu.caltech.android.views.AccelWaveformView;
import edu.caltech.android.weatherstation.R;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Fragment for displaying accelerometer waveform view.
 */
public class AccelWaveformFragment extends Fragment implements RefreshableFragment {
    private static String TAG = "AccelWaveformFragment";
    private AccelWaveformView accelWaveformView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.local_accel_fragment, container, false);
        accelWaveformView = (AccelWaveformView) view.findViewById(R.id.accelWaveView);
        return view;
    }

    public void refreshView() {
        accelWaveformView.postInvalidate();
    }

    public void setSensorId(long l) {
        accelWaveformView.setSensorId(l);
    }

    public void changeColor(String string) {
        accelWaveformView.changeBackgroundColor(string);
    }
}
