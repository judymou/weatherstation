package edu.caltech.android.fragments;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentSkipListSet;

import edu.caltech.android.datacenters.WeatherDataCenter;
import edu.caltech.android.extra.WeatherForecastInfo;
import edu.caltech.android.fragments.interfaces.RefreshableFragment;
import edu.caltech.android.weatherstation.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Fragment for displaying weather forecast data.
 */
public class WeatherForecastFragment extends Fragment implements RefreshableFragment {
    private static String TAG = "WeatherForecastFragment";
    private View view;

    private int viewIndex;
    private WeatherDataCenter weatherData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView");
        View v = inflater.inflate(R.layout.weather_forecast, container, false);
        view = v;

        weatherData = WeatherDataCenter.getInstance();
        weatherData.alertNewData();
        return v;
    }

    public void setViewIndex(int v) {
        viewIndex = v;
    }

    public void refreshView() {
        refreshViewFiveDay();
    }

    private void refreshViewTwoDay() {
        if (!weatherData.getNewWeatherForcastAvail(viewIndex)) {
            return;
        }

        Iterator<WeatherForecastInfo> iter = weatherData.getWeatherForecastInfos(viewIndex)
                .iterator();
        List<WeatherForecastInfo> w = new ArrayList<WeatherForecastInfo>();
        while (iter.hasNext()) {
            w.add(iter.next());
        }
        ((TextView) view.findViewById(R.id.first_day)).setText("  " + w.get(0).getDay());
        ((TextView) view.findViewById(R.id.second_day)).setText("  " + w.get(1).getDay());
        String pkgName = getActivity().getPackageName();
        view.findViewById(R.id.day_1).setBackgroundResource(
                getResources()
                        .getIdentifier("d" + w.get(0).getConditionCode(), "drawable", pkgName));
        view.findViewById(R.id.day_2).setBackgroundResource(
                getResources()
                        .getIdentifier("d" + w.get(1).getConditionCode(), "drawable", pkgName));
        ((TextView) view.findViewById(R.id.first_t)).setText(w.get(0).getTempLow() + "-" +
                w.get(0).getTempHigh() + (char) 0x00B0);
        ((TextView) view.findViewById(R.id.second_t)).setText(w.get(1).getTempLow() + "-" +
                w.get(1).getTempHigh() + (char) 0x00B0);

    }

    private void refreshViewFiveDay() {
        if (!weatherData.getNewWeatherForcastAvail(viewIndex)) {
            return;
        }

        Iterator<WeatherForecastInfo> iter = weatherData.getWeatherForecastInfos(viewIndex)
                .iterator();
        List<WeatherForecastInfo> w = new ArrayList<WeatherForecastInfo>();
        while (iter.hasNext()) {
            w.add(iter.next());
        }
        ((TextView) view.findViewById(R.id.d1)).setText("  " + w.get(0).getDay());
        ((TextView) view.findViewById(R.id.d2)).setText("  " + w.get(1).getDay());
        ((TextView) view.findViewById(R.id.d3)).setText("  " + w.get(2).getDay());
        ((TextView) view.findViewById(R.id.d4)).setText("  " + w.get(3).getDay());
        ((TextView) view.findViewById(R.id.d5)).setText("  " + w.get(4).getDay());

        String pkgName = getActivity().getPackageName();
        ((ImageView) view.findViewById(R.id.w1)).setImageResource(getResources().getIdentifier(
                "d" + w.get(0).getConditionCode(), "drawable", pkgName));
        ((ImageView) view.findViewById(R.id.w2)).setImageResource(getResources().getIdentifier(
                "d" + w.get(1).getConditionCode(), "drawable", pkgName));
        ((ImageView) view.findViewById(R.id.w3)).setImageResource(getResources().getIdentifier(
                "d" + w.get(2).getConditionCode(), "drawable", pkgName));
        ((ImageView) view.findViewById(R.id.w4)).setImageResource(getResources().getIdentifier(
                "d" + w.get(3).getConditionCode(), "drawable", pkgName));
        ((ImageView) view.findViewById(R.id.w5)).setImageResource(getResources().getIdentifier(
                "d" + w.get(4).getConditionCode(), "drawable", pkgName));

        ((TextView) view.findViewById(R.id.t1)).setText(w.get(0).getTempLow() + "-" +
                w.get(0).getTempHigh() + (char) 0x00B0);
        ((TextView) view.findViewById(R.id.t2)).setText(w.get(1).getTempLow() + "-" +
                w.get(1).getTempHigh() + (char) 0x00B0);
        ((TextView) view.findViewById(R.id.t3)).setText(w.get(2).getTempLow() + "-" +
                w.get(2).getTempHigh() + (char) 0x00B0);
        ((TextView) view.findViewById(R.id.t4)).setText(w.get(3).getTempLow() + "-" +
                w.get(3).getTempHigh() + (char) 0x00B0);
        ((TextView) view.findViewById(R.id.t5)).setText(w.get(4).getTempLow() + "-" +
                w.get(4).getTempHigh() + (char) 0x00B0);
    }
}