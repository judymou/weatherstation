package edu.caltech.android.fragments;

import java.util.List;

import edu.caltech.android.datacenters.WeatherDataCenter;
import edu.caltech.android.fragments.interfaces.RefreshableFragment;
import edu.caltech.android.fragments.interfaces.RotatableFragment;
import edu.caltech.android.weatherstation.R;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Fragment for displaying weather data.
 */
public class ListViewFragment extends Fragment implements RefreshableFragment, RotatableFragment {
    private ListView listView;

    private WeatherDataCenter weatherData;
    private int showWeatherInteger;
    private List<String> weatherInfo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_view_fragment, container, false);
        listView = (ListView) view.findViewById(R.id.listView);

        weatherData = WeatherDataCenter.getInstance();
        weatherInfo = weatherData.getWeatherInfos();
        ColoredAdapter adapter = new ColoredAdapter(getActivity(),
                android.R.layout.simple_list_item_1, weatherInfo);
        listView.setAdapter(adapter);
        showWeatherInteger = -1;
        return view;
    }

    public void refreshView() {
        if (weatherData.getNewWeatherInfoAvail()) {
            weatherInfo = weatherData.getWeatherInfos();
            ColoredAdapter adapter = new ColoredAdapter(getActivity(),
                    android.R.layout.simple_list_item_1, weatherInfo);
            listView.setAdapter(adapter);
        }
    }

    public void rotateView() {
        int newIndex = showWeatherInteger + 1;
        if (newIndex >= listView.getCount() - 1) {
            newIndex = 0;
        }
        rotateView(newIndex);
    }

    public void rotateView(int i) {
        if (showWeatherInteger != i) {
            showWeatherInteger = i;
            ColoredAdapter adapter = new ColoredAdapter(getActivity(),
                    android.R.layout.simple_list_item_1, weatherInfo);
            listView.setAdapter(adapter);
            listView.setSelection(showWeatherInteger + 1);
        }
    }

    // -----------------------Color Adaptor----------------------
    private class ColoredAdapter extends ArrayAdapter<String> {
        private int color = Color.parseColor("#6BA5E7");
        private int colorOther = Color.parseColor("#FFFFFF");

        public ColoredAdapter(Context context, int textViewResourceId, List<String> objects) {
            super(context, textViewResourceId, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = super.getView(position, convertView, parent);

            if (position == showWeatherInteger + 1) {
                view.setBackgroundColor(color);
                return view;
            }
            view.setBackgroundColor(colorOther);

            return view;
        }
    }

}
