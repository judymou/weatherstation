package edu.caltech.android.comm;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import edu.caltech.android.extra.SensorInfo;
import edu.caltech.android.extra.Utility;
import edu.caltech.android.ntp.SntpClient;
import edu.caltech.saf.messages.Common.ClientSignature;
import edu.caltech.saf.messages.Common.SensorType;
import edu.caltech.saf.messages.Common.StatusType;
import edu.caltech.saf.messages.Event.EventMessage;
import edu.caltech.saf.messages.Event.EventMessage.SensorEvent;
import edu.caltech.saf.messages.Event.EventResponse;
import edu.caltech.saf.messages.Heartbeat.HeartbeatMessage;
import edu.caltech.saf.messages.Heartbeat.HeartbeatMessage.SensorEventRate;

/**
 * CloudClient is a singleton class that is responsible for communicating with the server. 
 * It encapsulates client information and logic that are required for the communication. 
 * Other components can send data to the server using a set of methods CloudClient provides. 
 * For example, Service can report anomalies to the server by calling sendPick method, passing 
 * the sensor type and the current value as arguments.
 */
public class CloudClient {
    // tag for logging, debugging, etc.
    private static final String TAG = "CloudClient";

    // URLs for communication with AppEngine
    private String POST_ADDRESS;
    private String UPDATE_ADDRESS;
    private String PICK_ADDRESS;
    private String HEARTBEAT_ADDRESS;

    private long clientId;
    private String clientSecret;
    private List<Long> sensorIds;
    private long latestRequestId;
    private int messageId;

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

    private SntpClient sntpClient;
    private boolean checkNTPThreadBool;
    public Thread checkNTPThread;
    private final int NTP_TIMEOUT = 2000; // 2 seconds in milliseconds
    private final long CHECK_NTP_INTERVAL = 1000L * 30L; // 10 minutes in milliseconds
    public static String NTP_Server = "ntp-01.caltech.edu";
    private volatile int timeDifferenceNTP;

    public CloudClient() {
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        sntpClient = new SntpClient();
        POST_ADDRESS = "http://csn-server.appspot.com/api/v1/test";
        UPDATE_ADDRESS = POST_ADDRESS + "/client/update";
        PICK_ADDRESS = POST_ADDRESS + "/event";
        HEARTBEAT_ADDRESS = POST_ADDRESS + "/heartbeat";
        messageId = 0;
        checkNTPThreadBool = true;
    }

    public CloudClient(long cId, String cSecret, List<Long> sIds, long lRequestId) {
        this();
        Log.d(TAG, "Start cloud client with existing client id: " + cId + " client secret: " +
                cSecret + " sensor Ids: " + sIds.toString() + " latest request: " + lRequestId);
        clientId = cId;
        clientSecret = cSecret;
        sensorIds = sIds;
        latestRequestId = lRequestId;

        // update default URL with clientId
        UPDATE_ADDRESS = UPDATE_ADDRESS + "/" + clientId;
        PICK_ADDRESS = PICK_ADDRESS + "/" + clientId;
        HEARTBEAT_ADDRESS = HEARTBEAT_ADDRESS + "/" + clientId;
    }
 
    public void sendPick(long sensorId, SensorType sensorType, SensorInfo p, Location loc,
            int eventCount, int windowSize) {
        Log.d(TAG, "Sending pick");

        ClientSignature clientSignature = createClientSignature();

        SensorEvent sensorEvent = SensorEvent.newBuilder().setSensorId(sensorId)
                .setSensorType(sensorType).addAllReadings(p.getAllReadings())
                .setEventCount(eventCount).setTimeWindow(windowSize).build();
        EventMessage eventMessage = EventMessage.newBuilder().setClientSignature(clientSignature)
                .setLatitude(loc.getLatitude()).setLongitude(loc.getLongitude())
                .setEvent(sensorEvent).build();

        new PickTask().execute(eventMessage);
        messageId++;

    }

    private class PickTask extends AsyncTask<EventMessage, Void, Void> {
        protected Void doInBackground(EventMessage... params) {
            EventMessage pickMsg = params[0];
            try {
                InputStream postResult = Utility.performPost(PICK_ADDRESS, pickMsg);

                if (postResult != null) {
                    EventResponse pickResp = EventResponse.parseFrom(postResult);
                    if (pickResp.getStatus().getType() == StatusType.SUCCESS) {
                        Log.d("PickTask", "Successfully sent pick to " + PICK_ADDRESS);
                    } else {
                        Log.d("PickTask", "Status: " + pickResp.getStatus().getType() + " Mesg: " +
                                pickResp.getStatus().getMessage());
                    }
                } else {
                    Log.d("PickTask", "Received null msg");
                }

            } catch (IOException e) {
                Log.d("PickTask", Log.getStackTraceString(e));
            }
            return null;
        }
    }

    public void sendHearbeat(Map<Long, Integer> eventCount, Map<Long, Integer> timeWindow,
            Location loc) {
        Log.d(TAG, "Sending heartbeat");

        ClientSignature clientSignature = createClientSignature();
        List<SensorEventRate> sensorEventRates = new ArrayList<SensorEventRate>();
        for (Long l : eventCount.keySet()) {
            sensorEventRates.add(SensorEventRate.newBuilder().setEventCount(eventCount.get(l))
                    .setTimeWindow(timeWindow.get(l)).setSensorId(l).build());
        }

        HeartbeatMessage heartbeatMessage = HeartbeatMessage.newBuilder()
                .setClientSignature(clientSignature).setLatitude(loc.getLatitude())
                .setLongitude(loc.getLongitude()).addAllEventRates(sensorEventRates).build();
        new HeartbeatTask().execute(heartbeatMessage);
        messageId++;
    }

    private class HeartbeatTask extends AsyncTask<HeartbeatMessage, Void, Void> {
        protected Void doInBackground(HeartbeatMessage... params) {
            HeartbeatMessage heartbeatMsg = params[0];
            try {
                InputStream postResult = Utility.performPost(HEARTBEAT_ADDRESS, heartbeatMsg);

                if (postResult != null) {
                    EventResponse heartbeatResp = EventResponse.parseFrom(postResult);
                    if (heartbeatResp.getStatus().getType() == StatusType.SUCCESS) {
                        Log.d("HeartbeatTask", "Successfully sent heartbeat to " +
                                HEARTBEAT_ADDRESS);
                    } else {
                        Log.d("HeartbeatTask", "Status: " + heartbeatResp.getStatus().getType() +
                                " Mesg: " + heartbeatResp.getStatus().getMessage());
                    }
                } else {
                    Log.d("HeartbeatTask", "Received null msg");
                }

            } catch (IOException e) {
                Log.d("HeartbeatTask", Log.getStackTraceString(e));
            }
            return null;
        }
    }

    private ClientSignature createClientSignature() {
        Date date = new Date(System.currentTimeMillis() - timeDifferenceNTP * 1000);
        String dateString = new StringBuilder(dateFormat.format(date)).toString();
        String hashString = Utility.createSHA256HashString(dateString + "/" + messageId + "/" +
                clientSecret);
        ClientSignature clientSignature = ClientSignature.newBuilder().setDate(dateString)
                .setMessageId(messageId).setSignature(hashString).build();
        return clientSignature;
    }

    public void createNTPThread() {
        Log.d(TAG, "----------------Creating NTP thread-----------------");
        checkNTPThread = new Thread() {
            @Override
            public void run() {
                while (checkNTPThreadBool) {
                    try {
                        Log.d(TAG, "*****in while**********");
                        if (sntpClient.requestTime(NTP_Server, NTP_TIMEOUT)) {
                            long now = sntpClient.getNtpTime();
                            long systemnow = System.currentTimeMillis();

                            timeDifferenceNTP = (int) ((systemnow - now) / 1000L);
                            Log.d(TAG, "timeDifferentNTP: " + timeDifferenceNTP);
                        } else {
                            Log.d(TAG, "*****request time was not successful**********");
                        }
                        Thread.sleep(CHECK_NTP_INTERVAL);
                    } catch (Exception e) {
                        Log.d(TAG, "Exception requesting time from NTP " + e);
                    }
                }
            }
        };

        Log.d(TAG, "starting checkNTPThread");
        checkNTPThread.start();
    }

    public void stop() {
        checkNTPThreadBool = false;
    }

    public int getMessageId() {
        return messageId;
    }
    
    public void setMessageId(int mId) {
        messageId = mId;
    }
    
    // For debugging only
    public long getClientId() {
        return clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }
    
}
