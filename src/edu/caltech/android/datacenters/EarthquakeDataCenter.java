package edu.caltech.android.datacenters;

import java.util.ArrayList;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.PriorityBlockingQueue;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import edu.caltech.android.datacenters.interfaces.DataAvailabilityUpdater;
import edu.caltech.android.extra.QuakeInfo;

import android.util.Log;

/**
 * Singleton class to store earthquake data.
 */
public class EarthquakeDataCenter implements DataAvailabilityUpdater {
    private static String TAG = "EarthquakeData";
    private static EarthquakeDataCenter instance = new EarthquakeDataCenter();
    private ConcurrentSkipListSet<QuakeInfo> quakes;
    private boolean newDataAvail;

    private EarthquakeDataCenter() {
        super();
        quakes = new ConcurrentSkipListSet<QuakeInfo>();
        newDataAvail = false;
    }

    public static EarthquakeDataCenter getInstance() {
        return instance;
    }

    public boolean addQuake(QuakeInfo q) {
        if (quakes.contains(q)) {
            return false;
        } else {
            // Log.i(TAG, "adding new quake: date: " + q.getDate().toString() +
            // " lat: " + q.getLocation().getLatitude() + "lng: " +
            // q.getLocation().getLongitude());
            quakes.add(q);
            return true;
        }
    }

    public void deleteOldQuakes() {
        DateTime timeNow = new DateTime(DateTimeZone.UTC);
        DateTime time = timeNow.minusDays(7);
        while (quakes.isEmpty() == false && quakes.first().getDate().compareTo(time) < 0) {
            QuakeInfo q = quakes.pollFirst();
            // Log.i(TAG, "deleting new quake: date: " + q.getDate().toString()
            // +
            // " lat: " + q.getLocation().getLatitude() + "lng: " +
            // q.getLocation().getLongitude());
        }
    }

    public SortedSet<QuakeInfo> getQuakes() {
        return Collections.unmodifiableSortedSet(quakes);
    }

    public List<String> getRecentQuakes() {
        List<String> recentQuakeString = new ArrayList<String>();
        recentQuakeString.add("Recent Earthquakes");

        Iterator<QuakeInfo> iter = quakes.descendingIterator();
        while (iter.hasNext()) {
            if (recentQuakeString.size() > 3) {
                break;
            }
            recentQuakeString.add(iter.next().toString());
        }
        return recentQuakeString;
    }

    public void alertNewData() {
        newDataAvail = true;
    }

    public boolean getNewDataAvail() {
        if (newDataAvail) {
            newDataAvail = false;
            return true;
        } else {
            return false;
        }
    }
}
