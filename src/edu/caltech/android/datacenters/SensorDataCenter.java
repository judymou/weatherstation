package edu.caltech.android.datacenters;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentSkipListSet;

import edu.caltech.android.datacenters.interfaces.DataAvailabilityUpdater;
import edu.caltech.android.extra.SensorInfo;

import android.util.Log;

/**
 * Singleton class to store all sensor data.
 */
public class SensorDataCenter implements DataAvailabilityUpdater {

    private static final String TAG = "DataCenter";

    private boolean newDataAvail;

    private static SensorDataCenter instance;
    private HashMap<Long, ConcurrentSkipListSet<SensorInfo>> data;
    private HashMap<Long, Double> dataInit;

    private SensorDataCenter() {
        data = new HashMap<Long, ConcurrentSkipListSet<SensorInfo>>();
        dataInit = new HashMap<Long, Double>();
    }

    public static SensorDataCenter getInstance() {
        if (instance == null) {
            instance = new SensorDataCenter();
        }
        return instance;
    }

    public void init(List<Long> sensorIds) {
        data.clear();
        for (Long l : sensorIds) {
            data.put(l, new ConcurrentSkipListSet<SensorInfo>());
        }
    }

    // Because sensor data is always updating, these two methods are
    // not used currently. In case of sensor stops sending data, these
    // two methods could be very useful.
    @Override
    public void alertNewData() {
        newDataAvail = true;
    }

    @Override
    public boolean getNewDataAvail() {
        if (newDataAvail) {
            newDataAvail = false;
            return true;
        } else {
            return false;
        }
    }

    public int getSize() {
        return data.size();
    }

    public void initData(Long id, double value) {
        dataInit.put(id, value);
    }

    public Double getDataInit(Long id) {
        return dataInit.get(id);
    }

    public void addSample(Long id, SensorInfo info, int windowMilliseconds, int plotSamplesPerSecond) {

        double sampleSpacingMillis = 1000 / plotSamplesPerSecond;

        // Only add sample if some amount of time has elapsed since the
        // previous sample, e.g. downsample.
        long previousTime = System.currentTimeMillis() - windowMilliseconds;
        ConcurrentSkipListSet<SensorInfo> samples = data.get(id);
        if (samples == null) {
            // Log.i(TAG, "trying to add to sensor queue " + id +
            // " but it's not set up yet");
            return;
        }

        if (!samples.isEmpty()) {
            previousTime = samples.last().getTime();
        }

        if (info.getTime() > previousTime + sampleSpacingMillis) {
            samples.add(info);
        }

        // only keep windowMilliseconds of data
        if (!samples.isEmpty()) {
            long latest = samples.last().getTime();
            long staleTime = latest - windowMilliseconds;
            while (!samples.isEmpty() && samples.first().getTime() < staleTime) {
                samples.pollFirst();
            }
        }
    }

    public SortedSet<SensorInfo> getSamples(long id) {
        if (data.get(id) == null) {
            return null;
        } else {
            return Collections.unmodifiableSortedSet(data.get(id));
        }

    }
}
