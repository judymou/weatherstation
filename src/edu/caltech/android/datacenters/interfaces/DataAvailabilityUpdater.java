package edu.caltech.android.datacenters.interfaces;

/**
 *  Data providers can use the interface to indicate availability of new data, 
 *  while data consumers can check whether new data is available for display.
 *  This interface allows consumers to check for changed data before performing 
 *  a UI refresh instead of wasting processing time on rerendering old information.
 */
public interface DataAvailabilityUpdater {
    public void alertNewData();

    public boolean getNewDataAvail();
}
