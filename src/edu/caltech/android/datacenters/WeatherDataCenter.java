package edu.caltech.android.datacenters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;

import edu.caltech.android.datacenters.interfaces.DataAvailabilityUpdater;
import edu.caltech.android.extra.WeatherForecastInfo;
import edu.caltech.android.extra.WeatherInfo;

import android.util.Log;

/**
 * Singleton class to store weather data.
 */
public class WeatherDataCenter implements DataAvailabilityUpdater {
    private static String TAG = "WeatherData";
    private static WeatherDataCenter instance = new WeatherDataCenter();
    private ConcurrentSkipListSet<WeatherInfo> weatherInfos;
    private ConcurrentHashMap<Integer, ConcurrentSkipListSet<WeatherForecastInfo>> weatherForecastInfos;

    private boolean weatherInfoNew;
    private Map<Integer, Boolean> weatherForecastInfoNew;

    private WeatherDataCenter() {
        super();
        weatherInfos = new ConcurrentSkipListSet<WeatherInfo>();
        weatherForecastInfos = new ConcurrentHashMap<Integer, ConcurrentSkipListSet<WeatherForecastInfo>>();

        weatherInfoNew = false;
        weatherForecastInfoNew = new HashMap<Integer, Boolean>();
    }

    public static WeatherDataCenter getInstance() {
        Log.i(TAG, "Returnning earthquake data instance: " + instance);
        return instance;
    }

    public void clear() {
        weatherInfos.clear();
        weatherForecastInfos.clear();
    }

    public void add(WeatherInfo s) {
        weatherInfos.add(s);
    }

    public void addForecast(int index, ConcurrentSkipListSet<WeatherForecastInfo> w) {
        // map cityIndex to city weather forecast
        // cityIndex indicate the order or city shown
        weatherForecastInfos.put(index, w);
    }

    public List<String> getWeatherInfos() {
        ArrayList<String> weatherInfoString = new ArrayList<String>();
        weatherInfoString.add("Weather");
        for (WeatherInfo w : weatherInfos) {
            weatherInfoString.add(w.toString());
        }
        return weatherInfoString;
    }

    public SortedSet<WeatherForecastInfo> getWeatherForecastInfos(int cityIndex) {
        return Collections.unmodifiableSortedSet(weatherForecastInfos.get(cityIndex));
    }

    public void alertNewData() {
        weatherInfoNew = true;
        for (int i : weatherForecastInfos.keySet()) {
            weatherForecastInfoNew.put(i, true);
        }
    }

    public boolean getNewDataAvail() {
        boolean newData = false;
        for (int i : weatherForecastInfos.keySet()) {
            newData |= getNewWeatherForcastAvail(i);
        }
        newData |= getNewWeatherInfoAvail();
        return newData;
    }

    public boolean getNewWeatherInfoAvail() {
        if (weatherInfoNew) {
            weatherInfoNew = false;
            return true;
        } else {
            return false;
        }
    }

    public boolean getNewWeatherForcastAvail(int cityIndex) {
        Boolean newData = weatherForecastInfoNew.get(cityIndex);
        if (newData != null && newData) {
            weatherForecastInfoNew.put(cityIndex, false);
            return true;
        } else {
            return false;
        }
    }
}
