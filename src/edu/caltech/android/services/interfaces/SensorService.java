package edu.caltech.android.services.interfaces;

import edu.caltech.android.comm.CloudClient;
import edu.caltech.android.datacenters.SensorDataCenter;
import edu.caltech.android.extra.SensorInfo;
import edu.caltech.android.extra.SensorInfoThreeValues;
import edu.caltech.android.sensors.Localizer;
import edu.caltech.android.weatherstation.WeatherStationApplication;
import edu.caltech.saf.messages.Common.SensorType;
import android.app.Service;
import android.content.Intent;
import android.location.Location;

/**
 * This is the base class for the Service that processes sensor data. Since its
 * task includes storing data and sending abnormal data points to the cloud
 * server, the abstract class contains a SensorDataCenter instance and a
 * CloudClient instance. This abstract class also includes a Localizer instance
 * because the device location is required when delivering data to the server.
 * Device opening, closing, and reporting anomalies steps vary across different
 * sensor devices, so they are defined as abstract methods.
 */
public abstract class SensorService extends Service {
    // duration of the waveform shown.
    private int windowMilliseconds = 3000;
    // down-sample data to this rate for plotting
    private int plotSamplesPerSecond = 100;

    private SensorDataCenter dataCenter;
    private CloudClient cloudClient;
    private Localizer localizer;

    @Override
    public void onCreate() {
        dataCenter = SensorDataCenter.getInstance();
        localizer = new Localizer(getApplicationContext());
        cloudClient = ((WeatherStationApplication) getApplication()).getCloudClient();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        openDevice();
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        localizer.stop();
        closeDevice();
    }

    public abstract void openDevice();

    public abstract void closeDevice();

    // In case the service handle more than 1 sensor, also provide sensorIndex
    // specifying the sensor
    public abstract void reportPick(SensorInfo p, int sensorIndex);

    public void addSampleHelper(Long id, SensorInfo info) {
        dataCenter.addSample(id, info, windowMilliseconds, plotSamplesPerSecond);
    }

    public void reportPickHelper(Long id, SensorType type, SensorInfo p, int eventCount,
            int windowSize) {
        Location location = localizer.getLocation();
        cloudClient.sendPick(id, type, p, location, eventCount, windowSize);
    }

    public void setWindowMilliseconds(int w) {
        windowMilliseconds = w;
    }

    public void setPlotSamplesPerSecond(int s) {
        plotSamplesPerSecond = s;
    }

}
