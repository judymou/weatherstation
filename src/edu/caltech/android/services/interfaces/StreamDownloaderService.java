package edu.caltech.android.services.interfaces;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;

/**
 * This is the base class for the Service that downloads information from a data
 * stream (e.g. weather forecast and earthquake RSS feeds). In this case,
 * instead of processing the data points as they become available (usually via a
 * data listener), a timer-triggered task downloads information from a data
 * stream periodically. This abstract class sets up the timer and leaves only
 * the refreshStreamHelper method to be implemented by children classes, so
 * Services can perform their own data processing strategy.
 */
public abstract class StreamDownloaderService extends Service {
    private Timer updateTimer;
    private Handler handler = new Handler();
    private int updateFreq = 15 * 60 * 1000; // default to 15 min

    private RefreshStreamTask lastLookup;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        updateTimer = new Timer("updateTimer");
        updateTimer.scheduleAtFixedRate(new TimerTask() {
            public void run() {

                // Add the task to Android os task queue
                handler.post(new Runnable() {
                    public void run() {
                        refreshStream();
                    }
                });
            }
        }, 0, updateFreq);
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        updateTimer.cancel();
        handler.removeCallbacksAndMessages(null);
    }

    public void setUpdateFreq(double minite) {
        updateFreq = (int) (minite * 60 * 1000);
    }

    public void refreshStream() {
        if (lastLookup != null &&
                (lastLookup.getStatus().equals(AsyncTask.Status.PENDING) || lastLookup.getStatus()
                        .equals(AsyncTask.Status.RUNNING))) {
            // If the task is currently running, do not schedule another task to
            // avoid multiple same lookup task running the same time.
            return;
        }
        lastLookup = new RefreshStreamTask();
        lastLookup.execute((Void[]) null);
    }

    private class RefreshStreamTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            refreshStreamHelper();
            return null;
        }
    }

    public abstract void refreshStreamHelper();

}
