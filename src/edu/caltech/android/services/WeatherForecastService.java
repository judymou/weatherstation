package edu.caltech.android.services;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentSkipListSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import edu.caltech.android.datacenters.WeatherDataCenter;
import edu.caltech.android.extra.City;
import edu.caltech.android.extra.QuakeInfo;
import edu.caltech.android.extra.WeatherForecastInfo;
import edu.caltech.android.extra.WeatherInfo;
import edu.caltech.android.services.interfaces.StreamDownloaderService;
import edu.caltech.android.weatherstation.WeatherStationApplication;

/**
 * Service for downloading weather data from RSS feeds.
 */
public class WeatherForecastService extends StreamDownloaderService {
    private static final String TAG = "WeatherForcastService";
    private ArrayList<City> cities;

    private final IBinder binder = new WeatherForecastServiceBinder();

    private WeatherDataCenter weatherData;
    private double weatherForecastUpdateFreq = 60;

    @Override
    public void onCreate() {
        weatherData = WeatherDataCenter.getInstance();
        setUpdateFreq(weatherForecastUpdateFreq); // min
        cities = ((WeatherStationApplication) getApplication()).getCities();
    }

    @Override
    public void refreshStreamHelper() {
        weatherData.clear();
        for (int i = 0; i < cities.size(); i++) {
            City currentCityInfo = cities.get(i);
            String weatherString = QueryYahooWeather(currentCityInfo.getZipcode());
            Document weatherDoc = convertStringToDocument(weatherString);

            WeatherInfo weatherResult = parseWeather(weatherDoc, currentCityInfo);
            weatherData.add(weatherResult);

            ConcurrentSkipListSet<WeatherForecastInfo> weatherForecast = parseWeatherForecast(weatherDoc);
            weatherData.addForecast(currentCityInfo.getIndex(), weatherForecast);
        }
        weatherData.alertNewData();
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return binder;
    }

    public class WeatherForecastServiceBinder extends Binder {
        public WeatherForecastService getService() {
            return WeatherForecastService.this;
        }
    }

    private String QueryYahooWeather(int cityId) {
        String qResult = "";
        String queryString = "http://xml.weather.yahoo.com/forecastrss/" + cityId + "&d=5.xml";
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(queryString);

        try {
            HttpEntity httpEntity = httpClient.execute(httpGet).getEntity();

            if (httpEntity != null) {
                InputStream inputStream = httpEntity.getContent();
                Reader in = new InputStreamReader(inputStream);
                BufferedReader bufferedreader = new BufferedReader(in);
                StringBuilder stringBuilder = new StringBuilder();

                String stringReadLine = null;

                while ((stringReadLine = bufferedreader.readLine()) != null) {
                    stringBuilder.append(stringReadLine + "\n");
                }

                qResult = stringBuilder.toString();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return qResult;
    }

    private Document convertStringToDocument(String src) {
        Document dest = null;

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder parser;

        try {
            parser = dbFactory.newDocumentBuilder();
            dest = parser.parse(new ByteArrayInputStream(src.getBytes()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dest;
    }

    private WeatherInfo parseWeather(Document srcDoc, City cityInfo) {

        WeatherInfo myWeather = new WeatherInfo();
        if (srcDoc == null) {
            return myWeather;
        }
        myWeather.setCityInfo(cityInfo);
        // <yweather:location city="New York" region="NY"
        // country="United States"/>
        Node locationNode = srcDoc.getElementsByTagName("yweather:location").item(0);
        myWeather.setCity(locationNode.getAttributes().getNamedItem("city").getNodeValue()
                .toString());
        myWeather.setRegion(locationNode.getAttributes().getNamedItem("region").getNodeValue()
                .toString());
        myWeather.setCountry(locationNode.getAttributes().getNamedItem("country").getNodeValue()
                .toString());

        // <yweather:wind chill="60" direction="0" speed="0"/>
        Node windNode = srcDoc.getElementsByTagName("yweather:wind").item(0);
        myWeather.setWindChill(windNode.getAttributes().getNamedItem("chill").getNodeValue()
                .toString());
        myWeather.setWindDirection(windNode.getAttributes().getNamedItem("direction")
                .getNodeValue().toString());
        myWeather.setWindSpeed(windNode.getAttributes().getNamedItem("speed").getNodeValue()
                .toString());

        // <yweather:astronomy sunrise="6:52 am" sunset="7:10 pm"/>
        Node astronomyNode = srcDoc.getElementsByTagName("yweather:astronomy").item(0);
        myWeather.setSunrise(astronomyNode.getAttributes().getNamedItem("sunrise").getNodeValue()
                .toString());
        myWeather.setSunset(astronomyNode.getAttributes().getNamedItem("sunset").getNodeValue()
                .toString());

        // <yweather:condition text="Fair" code="33" temp="60"
        // date="Fri, 23 Mar 2012 8:49 pm EDT"/>
        Node conditionNode = srcDoc.getElementsByTagName("yweather:condition").item(0);
        myWeather.setConditiontext(conditionNode.getAttributes().getNamedItem("text")
                .getNodeValue().toString());
        myWeather.setConditiondate(conditionNode.getAttributes().getNamedItem("date")
                .getNodeValue().toString());

        return myWeather;
    }

    private ConcurrentSkipListSet<WeatherForecastInfo> parseWeatherForecast(Document srcDoc) {
        ConcurrentSkipListSet<WeatherForecastInfo> weather = new ConcurrentSkipListSet<WeatherForecastInfo>();
        for (int i = 0; i < 5; i++) {
            Node forecastNode = srcDoc.getElementsByTagName("yweather:forecast").item(i);
            WeatherForecastInfo w = new WeatherForecastInfo();
            w.setDay(forecastNode.getAttributes().getNamedItem("day").getNodeValue().toString());
            w.setTempLow(forecastNode.getAttributes().getNamedItem("low").getNodeValue().toString());
            w.setTempHigh(forecastNode.getAttributes().getNamedItem("high").getNodeValue()
                    .toString());
            w.setCondition(forecastNode.getAttributes().getNamedItem("text").getNodeValue()
                    .toString());
            w.setConditionCode(forecastNode.getAttributes().getNamedItem("code").getNodeValue()
                    .toString());
            w.setIndex(i);
            weather.add(w);
        }

        return weather;
    }

}
