package edu.caltech.android.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import edu.caltech.android.comm.CloudClient;
import edu.caltech.android.datacenters.SensorDataCenter;
import edu.caltech.android.extra.SensorInfo;
import edu.caltech.android.extra.SensorInfoThreeValues;
import edu.caltech.android.picking.KsigmaActivity;
import edu.caltech.android.sensors.Localizer;
import edu.caltech.android.services.interfaces.SensorService;
import edu.caltech.android.weatherstation.WeatherStationApplication;
import edu.caltech.saf.messages.Common.SensorType;

/**
 * A Service to process accelerometer data from the Android sensors.
 */
public class AndroidAccelSensorService extends SensorService {
    private static final String TAG = "AndroidAccelSensorService";

    public static final String SENSOR_NAME = WeatherStationApplication.ANDROID_ACCEL;
    public static final String BROADCAST_PICK_ACTION = "AndroidAccelSensorService.PICK";

    private long sensorId;
    private SensorType sensorType;
    private KsigmaActivity ksigmaX;
    private KsigmaActivity ksigmaY;
    private KsigmaActivity ksigmaZ;

    Intent pickIntent;

    private final IBinder binder = new AndroidAccelSensorServiceBinder();

    @Override
    public void onCreate() {
        super.onCreate();
        setWindowMilliseconds(3000);
        setPlotSamplesPerSecond(100);

        ksigmaX = new KsigmaActivity();
        ksigmaY = new KsigmaActivity();
        ksigmaZ = new KsigmaActivity();

        pickIntent = new Intent(BROADCAST_PICK_ACTION);

        sensorId = getSharedPreferences(WeatherStationApplication.PREFERENCE, 0).getLong(
                SENSOR_NAME, -1);
        sensorType = ((WeatherStationApplication) getApplication()).getSensorType(SENSOR_NAME);
    }

    @Override
    public void openDevice() {
        SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        Sensor accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(sensorEventListener, accelerometer,
                SensorManager.SENSOR_DELAY_FASTEST);
    }

    private final SensorEventListener sensorEventListener = new SensorEventListener() {

        @Override
        public void onSensorChanged(SensorEvent event) {
            float x = event.values[0]; // m/s^2
            float y = event.values[1]; // m/s^2
            float z = event.values[2]; // m/s^2

            long t = System.currentTimeMillis();

            SensorInfo accelSample = new SensorInfoThreeValues(t, x, y, z);
            addSampleHelper(sensorId, accelSample);

            if (ksigmaX.ksigmaAlgo(x) || ksigmaY.ksigmaAlgo(y) || ksigmaZ.ksigmaAlgo(z)) {
                reportPick(accelSample, 0);
            }
        }

        @Override
        public void onAccuracyChanged(Sensor arg0, int arg1) {

        }
    };

    @Override
    public void closeDevice() {
        SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensorManager.unregisterListener(sensorEventListener);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public class AndroidAccelSensorServiceBinder extends Binder {
        public AndroidAccelSensorService getService() {
            return AndroidAccelSensorService.this;
        }
    }

    @Override
    public void reportPick(SensorInfo p, int SensorIndex) {
        int eventCount = Math.max(Math.max(ksigmaX.getEventCount(), ksigmaY.getEventCount()),
                ksigmaZ.getEventCount());
        int windowSize = Math.max(Math.max(ksigmaX.getWindowSize(), ksigmaY.getWindowSize()),
                ksigmaZ.getWindowSize());
        reportPickHelper(sensorId, sensorType, p, eventCount, windowSize);
        sendBroadcast(pickIntent);
    }
}
