package edu.caltech.android.services;

import com.phidgets.PhidgetException;
import com.phidgets.SpatialPhidget;
import com.phidgets.event.AttachEvent;
import com.phidgets.event.AttachListener;
import com.phidgets.event.SpatialDataEvent;
import com.phidgets.event.SpatialDataListener;

import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import edu.caltech.android.extra.SensorInfo;
import edu.caltech.android.extra.SensorInfoThreeValues;
import edu.caltech.android.picking.KsigmaActivity;
import edu.caltech.android.services.interfaces.SensorService;
import edu.caltech.android.weatherstation.WeatherStationApplication;
import edu.caltech.saf.messages.Common.SensorType;

/**
 * Service for processing Phidget data.
 */
public class PhidgetSensorService extends SensorService {
    private static final String TAG = "PhidgetSensorService";
    public static final String SENSOR_NAME = WeatherStationApplication.PHIDGET;
    public static final String BROADCAST_PICK_ACTION = "PhidgetSensorService.PICK";

    private long sensorId;
    private SensorType sensorType;
    private SpatialPhidget spatial;
    private KsigmaActivity ksigmaX;
    private KsigmaActivity ksigmaY;
    private KsigmaActivity ksigmaZ;

    Intent pickIntent;

    private final IBinder binder = new PhidgetSensorServiceBinder();

    @Override
    public void onCreate() {
        super.onCreate();

        Log.i(TAG, "Starting Phidget Activity");
        setWindowMilliseconds(3000);
        setPlotSamplesPerSecond(100);

        ksigmaX = new KsigmaActivity();
        ksigmaY = new KsigmaActivity();
        ksigmaZ = new KsigmaActivity();

        pickIntent = new Intent(BROADCAST_PICK_ACTION);
        sensorId = getSharedPreferences(WeatherStationApplication.PREFERENCE, 0).getLong(
                SENSOR_NAME, -1);
        sensorType = ((WeatherStationApplication) getApplication()).getSensorType(SENSOR_NAME);
    }

    @Override
    public void openDevice() {
        com.phidgets.usb.Manager.Initialize(this);
        try {
            spatial = new SpatialPhidget();
            spatial.addAttachListener(new AttachListener() {

                public void attached(AttachEvent ae) {
                    Log.i(TAG, "add attachListener");
                    try {
                        ((SpatialPhidget) ae.getSource()).setDataRate(4);
                    } catch (PhidgetException e) {
                        e.printStackTrace();
                    }
                }
            });
            spatial.addSpatialDataListener(new SpatialDataListener() {

                public void data(SpatialDataEvent sde) {

                    for (int j = 0; j < sde.getData().length; j++) {
                        if (sde.getData()[j].getAcceleration().length > 0) {
                            for (int i = 0; i < sde.getData()[j].getAcceleration().length; i++) {
                                float x = (float) sde.getData()[j].getAcceleration()[0]; // m/s^2
                                float y = (float) sde.getData()[j].getAcceleration()[1]; // m/s^2
                                float z = (float) sde.getData()[j].getAcceleration()[2]; // m/s^2
                                long t = System.currentTimeMillis();

                                SensorInfo accelSample = new SensorInfoThreeValues(t, x, y, z);
                                addSampleHelper(sensorId, accelSample);

                                if (ksigmaX.ksigmaAlgo(x) || ksigmaY.ksigmaAlgo(y) ||
                                        ksigmaZ.ksigmaAlgo(z)) {
                                    reportPick(accelSample, 0);
                                }
                            }
                        }
                    }
                }
            });
            spatial.openAny();
        } catch (PhidgetException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void closeDevice() {
        com.phidgets.usb.Manager.Uninitialize();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public class PhidgetSensorServiceBinder extends Binder {
        public PhidgetSensorService getService() {
            return PhidgetSensorService.this;
        }
    }

    public void reportPick(SensorInfo p, int sensorIndex) {

        int eventCount = Math.max(Math.max(ksigmaX.getEventCount(), ksigmaY.getEventCount()),
                ksigmaZ.getEventCount());
        int windowSize = Math.max(Math.max(ksigmaX.getWindowSize(), ksigmaY.getWindowSize()),
                ksigmaZ.getWindowSize());
        reportPickHelper(sensorId, sensorType, p, eventCount, windowSize);
        sendBroadcast(pickIntent);
    }
}
