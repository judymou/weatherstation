package edu.caltech.android.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.location.Location;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import edu.caltech.android.comm.CloudClient;
import edu.caltech.android.datacenters.SensorDataCenter;
import edu.caltech.android.extra.SensorInfo;
import edu.caltech.android.extra.SensorInfoOneValue;
import edu.caltech.android.extra.SensorInfoThreeValues;
import edu.caltech.android.extra.SensorValueInfo;
import edu.caltech.android.picking.KsigmaActivity;
import edu.caltech.android.sensors.Localizer;
import edu.caltech.android.services.interfaces.SensorService;
import edu.caltech.android.weatherstation.WeatherStationApplication;
import edu.caltech.saf.messages.Common.SensorType;

/**
 * Service for processing data from weather station box.
 */
public class WeatherStationSensorService extends SensorService {

    private static final String TAG = "BoxSensorService";

    // Same order as when sending data from the box arduino
    public static final ArrayList<String> SENSOR_NAMES = new ArrayList<String>(Arrays.asList(
            WeatherStationApplication.BOX_TEMPERATURE, WeatherStationApplication.BOX_PRESSURE,
            WeatherStationApplication.BOX_HUMIDITY, WeatherStationApplication.BOX_OPTICAL,
            WeatherStationApplication.BOX_ACCEL_H, WeatherStationApplication.BOX_ACCEL_V,
            WeatherStationApplication.BOX_PHOTOVOLTAIC, WeatherStationApplication.BOX_CH4,
            WeatherStationApplication.BOX_LPG, WeatherStationApplication.BOX_CO,
            WeatherStationApplication.BOX_H2, WeatherStationApplication.BOX_GEIGER));

    public static final String BROADCAST_PICK_ACTION = "BoxSensorService.PICK";

    private UsbSerialDriver driver;
    private Boolean sensorReading;
    private List<Long> sensorId;
    private List<SensorType> sensorType;
    private List<KsigmaActivity> ksigma;

    Intent pickIntent;

    private final IBinder binder = new BoxSensorServiceBinder();

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "onCreate BoxSensorService Activity");

        setWindowMilliseconds(60000);
        setPlotSamplesPerSecond(100);

        sensorReading = true;

        pickIntent = new Intent(BROADCAST_PICK_ACTION);

        sensorId = new ArrayList<Long>();
        sensorType = new ArrayList<SensorType>();
        ksigma = new ArrayList<KsigmaActivity>();
        for (String s : SENSOR_NAMES) {
            Long id = getSharedPreferences(WeatherStationApplication.PREFERENCE, 0).getLong(s, -1);
            sensorId.add(id);
            sensorType.add(((WeatherStationApplication) getApplication()).getSensorType(s));
            ksigma.add(new KsigmaActivity());
        }
    }

    public void openDevice() {
        UsbManager mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        HashMap<String, UsbDevice> deviceList = mUsbManager.getDeviceList();

        for (String key : deviceList.keySet()) {
            UsbDevice d = deviceList.get(key);
            if (d != null && d.getVendorId() == 9025) {
                driver = UsbSerialProber.acquire(mUsbManager, d);
                if (driver != null) {
                    try {
                        driver.open();
                        driver.setBaudRate(9600);
                        Thread thread = new Thread(new BoxThread());
                        thread.start();
                    } catch (IOException e) {
                        Log.i(TAG, e.getStackTrace().toString());
                    }
                }
            }
        }
    }

    public void openDeviceTest() {
        Thread thread = new Thread(new BoxThreadTest());
        thread.start();
        return;
    }

    public void closeDevice() {
        sensorReading = false;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public class BoxSensorServiceBinder extends Binder {
        public WeatherStationSensorService getService() {
            return WeatherStationSensorService.this;
        }
    }

    public void reportPick(SensorInfo p, int sensorIndex) {
        // TODO: more alert than just a toast
        reportPickHelper(sensorId.get(sensorIndex), sensorType.get(sensorIndex), p,
                ksigma.get(sensorIndex).getEventCount(), ksigma.get(sensorIndex).getWindowSize());

        Toast toast = Toast.makeText(getApplicationContext(), SENSOR_NAMES.get(sensorIndex) +
                "picked", Toast.LENGTH_SHORT);
        toast.show();
    }

    private class BoxThreadTest implements Runnable {

        public void run() {
            Log.i(TAG, "On Running");
            int i = 0;
            while (sensorReading) {

                float sensorValues[] = new float[15];

                if (i % 2 == 0) {
                    sensorValues[0] = (float) (1.0 + 0.1);
                    sensorValues[1] = (float) 273.0 + 1;
                    sensorValues[2] = (float) 98506.0 + 1;
                    sensorValues[3] = (float) 3 + 1;
                    sensorValues[4] = (float) 36.0 + 1;
                } else {
                    sensorValues[0] = (float) (1.0 - 0.1);
                    sensorValues[1] = (float) 273.0 - 1;
                    sensorValues[2] = (float) 98506.0 - 1;
                    sensorValues[3] = (float) 3 - 1;
                    sensorValues[4] = (float) 36.0 - 1;
                }
                sensorValues[5] = (float) 0.20167;
                sensorValues[6] = (float) 1.021875;
                sensorValues[7] = (float) 49.0;
                sensorValues[8] = (float) 51.0;
                sensorValues[9] = (float) 136.0;
                sensorValues[10] = (float) 138.0;
                sensorValues[11] = (float) 161.0;
                sensorValues[12] = (float) 14629.0;
                SensorValueInfo sensorValueMsg = new SensorValueInfo(sensorValues);
                for (int sensorIndex = 0; sensorIndex < sensorId.size(); sensorIndex++) {
                    float value = sensorValueMsg.getAttribute(sensorIndex);
                    SensorInfo newValue = new SensorInfoOneValue(System.currentTimeMillis(), value);

                    addSampleHelper(sensorId.get(sensorIndex), newValue);
                    if (ksigma.get(sensorIndex).ksigmaAlgo(value)) {
                        reportPick(newValue, sensorIndex);
                    }
                }

                i++;
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class BoxThread implements Runnable {

        @Override
        public void run() {
            Log.i(TAG, "On BoxThread Running");

            byte[] signalBegin = new byte[] { 1, 1, 1, 1, 1 };
            try {
                driver.write(signalBegin, 200);
            } catch (IOException e) {
                Log.i(TAG, "error writing signal");
                e.printStackTrace();
            }

            // Use this method to convert the bytes of data obtained from
            // Arduino
            // into different datatypes
            int ret = 0, intbits = 0, loc = 0;
            byte[] buffer = new byte[4096];
            byte[] finalBuffer = new byte[300];
            float sensorValues[] = new float[15];
            while (sensorReading) {
                try {
                    ret = driver.read(buffer, 200);
                } catch (IOException e) {
                    break;
                }

                if (ret <= 0) {
                    continue;
                }
                for (int j = 0; j < ret; j++) {
                    finalBuffer[loc + j] = buffer[j];
                }

                loc += ret;
                // Log.i(TAG, "loc: " + loc + " ret: " + ret);
                if (loc >= 60) {

                    for (int i = 0; i < 60; i += 4) {
                        intbits = (finalBuffer[i + 3] << 24) | ((finalBuffer[i + 2] & 0xff) << 16) |
                                ((finalBuffer[i + 1] & 0xff) << 8) | (finalBuffer[i] & 0xff);
                        float t = Float.intBitsToFloat(intbits);
                        // Log.i(TAG, "t value is: " + t);
                        sensorValues[(int) (i / 4)] = t;
                    }

                    for (int i = 0; i < loc - 60; i++) {
                        finalBuffer[i] = finalBuffer[60 + i];
                    }
                    loc = loc - 60;

                    SensorValueInfo sensorValueMsg = new SensorValueInfo(sensorValues);
                    for (int sensorIndex = 0; sensorIndex < sensorId.size(); sensorIndex++) {
                        float value = sensorValueMsg.getAttribute(sensorIndex);
                        SensorInfo newValue = new SensorInfoOneValue(System.currentTimeMillis(),
                                value);

                        addSampleHelper(sensorId.get(sensorIndex), newValue);
                        if (ksigma.get(sensorIndex).ksigmaAlgo(value)) {
                            reportPick(newValue, sensorIndex);
                        }
                    }

                    byte[] signal = new byte[] { 1, 1, 1, 1, 1 };
                    try {
                        driver.write(signal, 200);
                    } catch (IOException e) {
                        Log.i(TAG, "error writing signal");
                        e.printStackTrace();
                    }
                }
            }
        }
    }

}
