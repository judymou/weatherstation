package edu.caltech.android.services;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Timer;
import java.util.TimerTask;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import edu.caltech.android.datacenters.EarthquakeDataCenter;
import edu.caltech.android.extra.QuakeInfo;
import edu.caltech.android.services.WeatherStationSensorService.BoxSensorServiceBinder;
import edu.caltech.android.services.interfaces.StreamDownloaderService;

/**
 * Service for getting quake events from USGS via RSS feed. Periodically
 * refreshes. The parser takes from
 * "Professional Android 2 Application Development"
 * 
 * Portions of this code from Professional Android 2 Application Development,
 * ISBN: 978-0-470-56552-0, copyright Wiley Publishing Inc: 2010, by Reto Meier,
 * published under the Wrox imprint are used by permission of Wiley Publishing,
 * Inc. All rights reserved. This book and the Wrox code are available for
 * purchase or download at www.wrox.com
 * 
 * @author mfaulk, judymou
 * 
 */
public class EarthquakeService extends StreamDownloaderService {
    private static final String TAG = "EarthquakeService";

    // 7 days urls
    // "http://earthquake.usgs.gov/earthquakes/catalogs/7day-M2.5.xml";
    private static final String QUAKE_URL = "http://earthquake.usgs.gov/earthquakes/catalogs/1day-M2.5.xml";

    private final IBinder binder = new EarthquakeServiceBinder();

    private EarthquakeDataCenter earthquakeData;
    private double earthquakeUpdateFreq = 60; // refresh earthquakeData hourly

    @Override
    public void onCreate() {
        super.onCreate();
        earthquakeData = EarthquakeDataCenter.getInstance();
        setUpdateFreq(earthquakeUpdateFreq);
    }

    @Override
    public void refreshStreamHelper() {
        // Get the XML, parse, and create Quakes.
        URL url;
        boolean newQuakeAdded = false;
        earthquakeData.deleteOldQuakes();
        int count = 0;
        try {
            String quakeFeed = QUAKE_URL;
            url = new URL(quakeFeed);
            URLConnection connection;
            connection = url.openConnection();
            HttpURLConnection httpConnection = (HttpURLConnection) connection;
            int responseCode = httpConnection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                Log.i(TAG, "quakeFeed connection ok");
                InputStream in = httpConnection.getInputStream();
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                // Parse the earthquake feed.
                Document dom = db.parse(in);
                Element docEle = dom.getDocumentElement();

                // Get a list of each earthquake entry.
                NodeList nl = docEle.getElementsByTagName("entry");
                if (nl != null && nl.getLength() > 0) {
                    for (int i = 0; i < nl.getLength(); i++) {
                        Element entry = (Element) nl.item(i);
                        Element title = (Element) entry.getElementsByTagName("title").item(0);
                        Element g = (Element) entry.getElementsByTagName("georss:point").item(0);
                        Element when = (Element) entry.getElementsByTagName("updated").item(0);
                        Element link = (Element) entry.getElementsByTagName("link").item(0);

                        String details = title.getFirstChild().getNodeValue();
                        String hostname = "http://earthquake.usgs.gov";
                        String linkString = hostname + link.getAttribute("href");
                        String point = g.getFirstChild().getNodeValue();
                        String dt = when.getFirstChild().getNodeValue();

                        // date
                        DateTimeFormatter sdf = DateTimeFormat
                                .forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
                        DateTime qdate = null;
                        qdate = sdf.withZone(DateTimeZone.UTC).parseDateTime(dt);

                        // location
                        String[] location = point.split(" ");
                        Location l = new Location("dummyGPS");

                        final double latitude = Double.parseDouble(location[0]);
                        final double longitude = Double.parseDouble(location[1]);

                        l.setLatitude(latitude);
                        l.setLongitude(longitude);

                        // magnitude
                        String magnitudeString = details.split(" ")[1];
                        int end = magnitudeString.length() - 1;
                        final double magnitude = Double.parseDouble(magnitudeString.substring(0,
                                end));

                        // details
                        details = details.split(",")[1].trim();
                        QuakeInfo quake = new QuakeInfo(qdate, details, l, magnitude, linkString);

                        // Process a newly found earthquake
                        boolean newQuake = earthquakeData.addQuake(quake);
                        newQuakeAdded |= newQuake;
                        count++;

                        // An optimization: assume that xml is in descending
                        // time order. If we encounter
                        // a quake that's already in the database, this implies
                        // that we don't have to continue
                        // because we have already had the rest in the database.
                        if (newQuake == false) {
                            break;
                        }
                    }
                }

            }
        } catch (MalformedURLException e) {
            Log.i(TAG, e.getStackTrace().toString());
        } catch (IOException e) {
            Log.i(TAG, e.getStackTrace().toString());
        } catch (ParserConfigurationException e) {
            Log.i(TAG, e.getStackTrace().toString());
        } catch (SAXException e) {
            Log.i(TAG, e.getStackTrace().toString());
        } finally {
        }

        if (newQuakeAdded) {
            earthquakeData.alertNewData();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public class EarthquakeServiceBinder extends Binder {
        public EarthquakeService getService() {
            return EarthquakeService.this;
        }
    }
}
