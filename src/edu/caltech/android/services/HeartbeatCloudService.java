package edu.caltech.android.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import edu.caltech.android.comm.CloudClient;
import edu.caltech.android.datacenters.SensorDataCenter;
import edu.caltech.android.extra.Utility;
import edu.caltech.android.sensors.Localizer;
import edu.caltech.android.weatherstation.RegistrationActivity;
import edu.caltech.android.weatherstation.WeatherStationApplication;

/**
 * Service for sending heartbeat message to CSN backend.
 */
public class HeartbeatCloudService extends Service {
    private static final String TAG = "CloudHeartBeatService";

    private final IBinder binder = new CloudHeartBeatServiceBinder();

    private SensorDataCenter dataCenter;
    private CloudClient cloudClient;
    private Localizer localizer;

    private Timer updateTimer;
    private Handler handler = new Handler();
    private int updateFreq;

    private List<Long> sensorIds;
    private Map<Long, Integer> eventCount;
    private Map<Long, Integer> timeWindow;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "onCreate CloudHeartBeatService Activity");
        dataCenter = SensorDataCenter.getInstance();
        localizer = new Localizer(getApplicationContext());
        updateFreq = 5; // min
        eventCount = new HashMap<Long, Integer>();
        timeWindow = new HashMap<Long, Integer>();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "onStart AccelSensorService Activity");

        // default eventCount and timeWindow
        SharedPreferences mPrefs = getSharedPreferences(WeatherStationApplication.PREFERENCE, 0);
        sensorIds = Utility.convertStringToList(mPrefs.getString(RegistrationActivity.SENSOR_IDS,
                "[]"));
        for (Long l : sensorIds) {
            eventCount.put(l, 5);
            timeWindow.put(l, 10);
        }

        cloudClient = ((WeatherStationApplication) getApplication()).getCloudClient();
        updateTimer = new Timer("heartbeatUpdate");
        updateTimer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        Log.i(TAG, "timer trigger heatbeat update");
                        reportHeartbeat();
                    }
                });
            }
        }, 0, updateFreq * 60 * 1000);

        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        localizer.stop();
        updateTimer.cancel();
        handler.removeCallbacksAndMessages(null);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public class CloudHeartBeatServiceBinder extends Binder {
        public HeartbeatCloudService getService() {
            return HeartbeatCloudService.this;
        }
    }

    private void reportHeartbeat() {
        Location location = localizer.getLocation();
        // TODO: use correct eventcount and timewindow
        cloudClient.sendHearbeat(eventCount, timeWindow, location);
    }
}
