package edu.caltech.android.picking;

import java.util.LinkedList;

import android.util.Log;

/**
 * This class implements an anomaly detection algorithm that uses the ratio of
 * short-term-average to long-term-average of data samples
 */
public class KsigmaActivity {

    private static final String TAG = "KsigmaActivity";

    private static final float EPSILON = 0.0001f;
    // If (sta - lt) / stddev > threshold, pick
    private static final double PICK_THRESHOLD = 4.5;
    private static final long PICK_GAP = 5 * 1000; // 5 seconds
    // Parameter should be adjusted for different sensor
    // targetDelta: the data rate
    // tLTA: number of seconds for calculating long term average (LTA)
    // sSTA: number of seconds for calculating short term average (STA)
    // tGAP: number of seconds between LTA and STA. For example, if LTA is
    // calculated from time a
    // to time b where b - a = tLTA. STA is calculated from b - tGAP - STA to b
    // - tGAP.
    private float targetDelta, tLTA, tSTA, tGap;

    // If the last pick time is within 2 seconds, do not send it again.
    private long lastPickTime;

    // Count number of picks per windowSize
    private int windowSize;
    private long lastWindowTime;
    private int prevEventCountInWindow = 0;
    private int currEventCountInWindow = 0;

    // From tLTA, tSTA, tGap, and data rate, calculate the number of samples in
    // each window
    private int nSamplesLTA, nSamplesSTA, gapLTA;

    private float shortTermSum = 0, longTermSum = 0, longTermSumSquare = 0;

    private LinkedList<Float> dataPointST;
    private LinkedList<Float> dataPointLT;
    private LinkedList<Float> dataPointGap;

    public KsigmaActivity() {
        targetDelta = 0.02f;
        tLTA = 10.0f;
        tSTA = 0.5f;
        tGap = 1.0f;
        windowSize = 10 * 60 * 1000; // 10 min;
        nSamplesLTA = (int) (tLTA / targetDelta);
        nSamplesSTA = (int) (tSTA / targetDelta);
        gapLTA = (int) (tGap / targetDelta);
        lastWindowTime = System.currentTimeMillis();
        dataPointST = new LinkedList<Float>();
        dataPointLT = new LinkedList<Float>();
        dataPointGap = new LinkedList<Float>();
    }

    public KsigmaActivity(float targetD, float lta, float sta, float gap, int ws) {
        targetDelta = targetD;
        tLTA = lta;
        tSTA = sta;
        tGap = gap;
        windowSize = ws;
        nSamplesLTA = (int) (tLTA / targetDelta);
        nSamplesSTA = (int) (tSTA / targetDelta);
        gapLTA = (int) (tGap / targetDelta);
        lastWindowTime = System.currentTimeMillis();
        dataPointST = new LinkedList<Float>();
        dataPointLT = new LinkedList<Float>();
        dataPointGap = new LinkedList<Float>();
    }

    public Boolean ksigmaAlgo(float value) {
        // If another time window pass, save the number of picks.
        long currTime = System.currentTimeMillis();
        if (currTime - lastWindowTime >= windowSize) {
            prevEventCountInWindow = currEventCountInWindow;
            currEventCountInWindow = 0;
            lastWindowTime = currTime;
        }

        // Start the algorithm only when we have enough data
        if (dataPointLT.size() + dataPointGap.size() >= nSamplesLTA + gapLTA) {
            Float firstLT = dataPointLT.removeFirst();
            Float firstGap = dataPointGap.removeFirst();
            Float firstST = dataPointST.removeFirst();

            longTermSum -= firstLT;
            longTermSumSquare -= firstLT * firstLT;
            shortTermSum -= firstST;

            dataPointLT.add(firstGap);
            dataPointGap.add(value);
            dataPointST.add(value);

            longTermSum += firstGap;
            longTermSumSquare += firstGap * firstGap;
            shortTermSum += value;

            Float shortTermAvg = shortTermSum / nSamplesSTA;
            Float longTermAvg = longTermSum / nSamplesLTA;
            Float longTermSquareAvg = longTermSumSquare / nSamplesLTA;
            Float stddev = EPSILON;
            if (longTermSquareAvg > (longTermAvg * longTermAvg)) {
                stddev = (float) Math.sqrt(longTermSquareAvg - (longTermAvg * longTermAvg));

                if (Math.abs(shortTermAvg - longTermAvg) / stddev > PICK_THRESHOLD &&
                        currTime - lastPickTime > PICK_GAP) {
                    currEventCountInWindow += 1;
                    lastPickTime = currTime;
                    return true;
                }
            }
        } else {
            if (dataPointLT.size() < nSamplesLTA) {
                dataPointLT.add(value);
                longTermSum += value;
                longTermSumSquare += value * value;
            } else {
                dataPointGap.add(value);
            }

            if (dataPointLT.size() + dataPointGap.size() > nSamplesLTA + gapLTA - nSamplesSTA) {
                dataPointST.add(value);
                shortTermSum += value;
            }
        }

        return false;
    }

    // Return window size in seconds
    public int getWindowSize() {
        return windowSize / 1000;
    }

    public int getEventCount() {
        return prevEventCountInWindow;
    }
}
